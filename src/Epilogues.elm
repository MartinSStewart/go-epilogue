module Epilogues exposing (..)


type alias Epilogue =
    { name : String
    , image : String
    , prerequisites : List String
    , author : String
    , story : List String
    }


gardelon : List Epilogue
gardelon =
    [ { name = "Sample Story 1"
      , image = "EmoteCecilia.png"
      , prerequisites = []
      , author = ""
      , story =
            [ "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ]
      }
    , { name = "Sample Story 2"
      , image = "EmoteCecilia.png"
      , prerequisites = []
      , author = ""
      , story =
            [ "page1"
            , "page2"
            , "page3"
            , "page4"
            ]
      }
    , { name = "Sample Story 4"
      , image = "EmoteCecilia.png"
      , prerequisites = []
      , author = ""
      , story =
            [ "page1"
            , "page2"
            , "page3"
            , "page4"
            ]
      }
    , { name = "Sample Story 5"
      , image = "EmoteCecilia.png"
      , prerequisites = []
      , author = ""
      , story =
            [ "page1"
            , "page2"
            , "page3"
            , "page4"
            ]
      }
    , { name = "Sample Story 6"
      , image = "EmoteCecilia.png"
      , prerequisites = [ "Sample Story 1", "Sample Story 5" ]
      , author = ""
      , story =
            [ "page1"
            , "page2"
            , "page3"
            , "page4"
            ]
      }
    , { name = "Sample Story 3"
      , image = "EmoteCecilia.png"
      , prerequisites = []
      , author = ""
      , story =
            [ "page1 page1"
            , "pageasdafwg2"
            , "page3 agawfag"
            , "page4 waijdiauhdiuahiudh"
            ]
      }
    ]


brittlepoint =
    [ { name = "And Ever", image = "AndEver.png", prerequisites = [], author = "", story = [ """
<p>
&emsp;&emsp;"Mark another mystery of the universe down," cheered Rose, helping her twin out of Brittlepoint Mohole. The sun was starting to break overhead, and the guards were starting to retire back to their beds after another diligent night watch. "This was a good one, I thought. Monsters, treasures, friendships, a little of everything."
</p>
<p>
 &emsp;&emsp;"Yeah."
</p>
<p>
 &emsp;&emsp;"And betrayals! We need to find the man who sold us that map. He must have known that we would end up face to face with that Dracodinohydra. He wanted us to die so that he could lay finder's claim to our loot!"
</p>
<p>
 &emsp;&emsp;"Definitely."
</p>
<p>
 &emsp;&emsp;"But now the people of the island can rest at least a bit better for a couple nights, at least until another monster that big comes along. I think we really made a difference today."
</p>
<p>
 &emsp;&emsp;"Right."
</p>
<p>
 &emsp;&emsp;Rose sighed and sat down next to Aura, who had seated herself with her feet hanging over the edge of the bottomless darkness they had just exited.
</p>
<p>
 &emsp;&emsp;"Okay, I'm invoking the three strike rule. You have to respond with more than one word this time. What's bothering you? I thought this adventure ruled. This was up there with finding the crown of the last king of Adamsale."
</p>
<p>
 &emsp;&emsp;When this evoked no response, she continued.
</p>
<p>
 &emsp;&emsp;"You don't have to enjoy things if you don't want to. I know things get really emotionally draining for you. But you <em>do </em>have to communicate that to me."
</p>
""", """
<p>
 &emsp;&emsp;Aura turned to her and slowly blinked one bright green eye, the other mechanical blue one keeping a steady gaze on Rose. Rose thought about how reassuring she always found the light of Aura's eye – just a few moments ago, climbing back out of the abyss of Brittlepoint Mohole, it had been a calming reminder that someone who loved her was right behind her and always would be.
</p>
<p>
 &emsp;&emsp;"Am enjoying things. Too much, maybe. Just got to thinking. How long can we keep going? Eventually will have to settle down somewhere. End the adventure. You'll want to be normal with someone."
</p>
<p>
 &emsp;&emsp;"Thanks for being open with me," Rose responded.
</p>
<p>
 &emsp;&emsp;She leaned her head on her twin's shoulder and put an arm around her, an action which was mimicked a second later by Aura. A few months ago she might have still felt offended by a comment like that, but she'd gotten good at forgiving Aura her accidental insults. Her twin was a much blunter person than she was, yet somehow much harder to get to admit what she was thinking. Chastising her when she opened up was counterproductive. Instead, Rose always chose to respond with the truth in kind. In this case, that felt very simple.
</p>
<p>
 &emsp;&emsp;"I will never, ever want to stop adventuring with you, Aura. There's not a single person in the world I'd rather be with than you. I'm always excited to visit the Fellowship, too, but even with them I'm thinking about getting back to it being just us. You and me will be together forever and ever and ever and ever and ever and..."
</p>
""", """

<p>
 &emsp;&emsp;Aura closed her eyes and let the echo of her sister's voice lull her into visions of the future, each so real that she had no question that they would come to pass, a series of scenes set years apart from each other with varied sceneries and friends but one invariable truth, that the same pair stood in the center of them all, hands gripped tight.
</p>""" ] } ]


shifting =
    [ { name = "Brand New Days", image = "AdmiralAlexis.png", prerequisites = [], author = "TS, RK, and MM", story = [ """
<p>
&emsp;&emsp;&emsp;&emsp;After the defeat of the gods on separate oceans of the world, the grand ocean would know peace once more, but a youth from the Fellowship knew that it needed to be secured. The world was very much still broken in places as a result of the World Marines, and she was intent on fixing it. Alexis knew this would be no small task, but endeavored on the path regardless for the sake of the world at large.
</p>
<p>
&emsp;&emsp;Twenty years of constant hard work pass, and each brand new day had entailed its own brand new adventure. Whether those adventures were reuniting people in the wake of the gods, making amends for the deeds of her predecessor, acting as a mediator in the pursuit of international peace, or, to Alexis’ dismay, mountains of paperwork. By then, she was known throughout the world as the Admiral of the World Marines. The organization once more went from a name to be feared and into a name to be cherished yet again due to her dedication to the betterment of the people of the world. For if there was one thing the determined girl never lost, it was her kindhearted spirit and her vision for a world in harmony.
</p>
<p>
&emsp;&emsp;One night aboard The Soul’s Journey, Alexis and the crew aboard the huge ship were all in the middle of sailing to an island in the Green Ocean that had been hit by a devastating hurricane. They carried food, medicine, construction materials, just about anything they could afford to bring relief to the island. Despite how close to midnight it was, all the busybodies on the ship had been running about to make the final preparations as they would have made landfall at first light the next day.
</p>
""", """
<p>
Alexis was no stranger in this regard, until she dropped what she had been carrying when a foul scent had made her stumble and hold her nose; all from none other than Spice Westward.
</p>
<p>
&emsp;&emsp;“Hullo! I told you, you should be turning in for the night, Admiral!”
</p>
<p>
&emsp;&emsp;“Stars above,” Alexis complained when she had let go of her nose, “I just thought I’d help with the last of the--”
</p>
<p>
&emsp;&emsp;“No!” Spice had interrupted when she pointed a finger up to Alexis, “You are overworked as is! Let the rest of us finish up. Or do you not trust Terremotto, Aell, Sahl, and the rest?”
</p>
<p>
&emsp;&emsp;Alexis looked down to Spice before she had nodded with quiet appreciation as she scratched her cheek with a smile. “No, of course not. I’m glad I have you all around.”
</p>
<p>
&emsp;&emsp;&emsp;&emsp;By the time Alexis had finally reached her office on the ship, it had been closer to midnight; as she had stopped several times to chat along the way. She had shut the doors behind him and left herself in the silence the office. That moment in time had felt surreal for her as she paced around, and looked at all the objects around she held near and dear to her on display around the room. Maybe that’s where that feeling had come from; that this was the totality of her world. How all of those little paths throughout her life led her to this moment of introspection.
</p>
""", """
<p>
&emsp;&emsp;It all started with her own sword. She still used the sword granted to her at her anointment by Cecilia, although it had been reforged to be better handled by her in her older age and taller form. All of her thoughts in that instance had gone back to home and to the Fellowship. Alexis remembered when Cecilia was terrified before Frederick and how, without a second thought, Alexis had sprung into action. There was nothing but pure fear in Alexis’ mind in that moment long ago, but she could have only done what she knew best; she held onto her valor when others couldn’t.
</p>
<p>
&emsp;&emsp;After she had put her sword up on a display rack, she went to sit down at her desk. Having opened one of the drawers, Alexis had retrieved an old envelope that was a letter from Eise. The letter was a brief piece of Eise having asked not to be contacted or seen by the Fellowship, having figured it would be a terrible idea for all involved. Despite the things Eise had done, she always held a special place in Alexis’ heart for having been one of the main pieces in Alexis’ quest for a sense of self. Alexis’ thoughts went to Captain Lullaby and the Singsong Pirates. Before them, Alexis had loathed herself, and she was plagued with thoughts that she was never good enough because she could not have understood what made her just as extraordinary as the rest. Without them, Alexis would have never found her true sense of worth. She would have never learned to truly love herself.
</p>
<p>
&emsp;&emsp;When she had put the envelope away, Alexis picked up what seemed to be some sort of action figure in the likeness of a Machineling that was faced outward from her desk. It was covered in signatures from the group of little kids that had gifted it to her.
</p>
""", """
<p>
One day, in the last couple of decades, Alexis had been back in Gardelon to help build a new orphanage. Even in that work, Alexis had never removed her armor. It was irritating to work in it, but she understood that it was part of her image as a shining knight whose strength and honor was trusted by children and the future of the seas. Alexis had activated the little talking mechanism on the toy and they spat back, <strong>“I MAY NOT HAVE A TEACHING DEGREE, BUT I’M ABOUT TO SCHOOL THESE CHUMPS.”</strong> This had made the weathered, but happy, admiral chuckle over its line, and caused her to feel tears well up. As if something about it held a special connection to her that transcended even the boundaries of the soul that Alexis had come to understand. That an immutable truth of the universe existed there, even though she had figured it would remain a mystery to her forever.
</p>
<p>
&emsp;&emsp;Having set the toy back down where it was, Alexis had then picked up a small box she held near and dear to her heart. Even though the same could be said for everything else she interacted with not long ago. It was a small box made entirely out of stone and, when opened, revealed a glimmering, green shard resting on purple satin. A piece that had fallen off of The Shard when Alexis had failed to pierce the Krathyd’s hide all on her own. She gently ran her thumb over the shard of The Shard when she had heard it.
</p>
<p>
&emsp;&emsp;“Alexis.”
</p>
<p>
&emsp;&emsp;He had called out to her, in that moment, when the Fellowship was rushing off to face down a god. She paused then, and turned back to him.
</p>
""", """
<p>
His eyes were fixed on his axe as she walked back to him, the twin blades flickering and glowing. At last he looked up.
</p>
<p>
&emsp;&emsp;“I think ye’ll need this,” he had said, holding the axe out to her. She had refused of course, but he had thrust the weapon into her hands. “It was born of the old gods. Might be able to help.”
</p>
<p>
&emsp;&emsp;At that moment in the past, Alexis did not know what to say. She had started stammering something when Wolsh placed his hand on the side of her face. The stone was rough, but he moved so gently, it caught her by surprise.
</p>
<p>
&emsp;&emsp;“Alexis,” he repeated, “I--”
</p>
<p>
&emsp;&emsp;But then words failed him too. He just looked at her, his gaze searching, and then their eyes met. He smiled at her, a warm, true smile. And then softly he pulled her head forward, until their foreheads met. His eyes closed and so did hers, and they stood there for a long moment, with nothing needed to be said.
</p>
<p>
&emsp;&emsp;Then he stepped away, the moment broken. “Go on now, lass. The world isnae gonna save itself. Go.”
</p>
<p>
&emsp;&emsp;And so she had left, and he was gone when she had returned.
</p>
<p>
&emsp;&emsp;&emsp;&emsp;Alexis had shut the small stone box when she was done thinking back to that one moment twenty years in the past. There had been a small sigh from her, but it was almost neutral in tone. No relief, but no sadness either. Needless to say, at that moment she had resolved that maybe it was time to bring that shard back home the next chance she got. To set it upon Wolsh’s cairn back in his homeland.
</p>
""", """
<p>
&emsp;&emsp;In her last moments of introspection at the time, Alexis had thought about what people in the future would think of her. She had hoped that she had done enough to distinguish the Marines under her command in comparison to under Diamonbody’s command. Despite everything in the world having given her a million reasons to believe it would be favorably, it still scared her a bit. After all, Alexis had been an orphan who had become a princess, bested The Breaker alongside King William in combat to save Gardelon, layed the Godslayer low, was the last one granted The Shard by Wolsh, became the new Admiral of the World Marines, and her then girlfriend was a dragon. Alexis believed that, at the very least, she’d go down as a historical figure that bordered an inspiring tall tale after a few centuries. Alas, even she knew that it was all up to the generations of the future to decide how she would be remembered by history.
</p>
<p>
&emsp;&emsp;Lastly in the night, there was a sound of the door into her office when it opened. Alexis had looked over as she placed the box next to the action figure once more, and then she had seen Adra standing at the door.
</p>
<p>
&emsp;&emsp;“Oh,” Alexis stammered for a moment, “Hey, honey--”
</p>
<p>
&emsp;&emsp;“Brooding in your office again?” Adra interrupted her.
</p>
<p>
&emsp;&emsp;“Ah, well-- You know how it goes.”
</p>
<p>
&emsp;&emsp;“Mhm,” Adra had admitted with a laugh, “C’mon, Alaina’s already asleep in her room, and you’ve got a busy day tomorrow!”
</p>
""", """
<p>
&emsp;&emsp;Alexis had stood from her desk and walked over to join Adra. This was the entirety of Alexis’ final thoughts for her night. How all those little paths led her to this moment. The totality of her own world. Through which Alexis had realized her calling, understood her own life and its paths, and obtained a sense of acceptance and absolution. Although Alexis had reached first, Adra was the one to take Alexis’ hand as two metallic bands clinked together from their fingers. For the night, the gods were in their heaven, and all was right with the world.
</p>""" ] } ]


gnehah =
    []


chasm =
    []


prehistoric =
    [ { name = "Fires in the Night"
      , image = "EmoteSilouhette.png"
      , prerequisites = []
      , author = ""
      , story =
            [ """
<p>
&emsp;&emsp;The last ever meeting of the Order of the Black Comet came on a cloudy, starless night. The best meteorologists among them had chosen the night specifically for how clear it was set to be on the unpopulated island they were to meet at, but fate had different plans. As the Matchstick Man had said as the group drifted apart for the last time, "At least it didn't end up pouring." The sentiment more or less summed up the gathering, everyone agreed.  
</p>
<p>
&emsp;&emsp;By one in the morning only two members of the former order remained. The first was the group's most enigmatic member, a man who had never provided his name to any of them and never would. He had a bundle of lit matches in his mouth and was holding the conversation with his companion by speaking through them, giving all of his words a muffled quality.  
</p>
<p>
&emsp;&emsp;The other was a scientist who went by the name of Dr. Nebulous. He was one of the most recent additions to the Order, having joined only a few years before the great war, though it wasn't until the war had ended that he had begun to fully devote himself to the Order's pursuits. In the Matchstick Man's opinion, Nebulous was one of the most advanced minds on the planet. Probably he rivaled even the group's own Tanis Silverhand. Not that Tanis counted as 'on the planet' anymore.  
</p>
<p>
&emsp;&emsp;"I never understood how you managed to keep those things burning so long," said Nebulous. He was leaning on his cane a bit more than usual tonight. "I light a match and it goes out in seconds." 
</p>
<p>
&emsp;&emsp;"Years of practice." 
</p>
""", """
<p>
&emsp;&emsp;"What a specific talent to hone. It can't be good for you, though."  
</p>
<p>
&emsp;&emsp;"What is, at this age?" He threw his head back and looked down his nose at the matches. "Hasn't killed me yet. I figure it's a science experiment at this point, just like you like. We're going to see if it'll actually kill me one day." 
</p>
<p>
&emsp;&emsp;"You know, there's something I've been wondering. Why are you in the Order? It doesn't seem like your thing."  
</p>
<p>
&emsp;&emsp;"Why <em>was</em> I in the order, you mean. And that's quite simple. I like stars." 
</p>
<p>
&emsp;&emsp;"That's it?" Nebulous found it hard to believe anyone would seek out such an elusive organization over something so trivial.  
</p>
<p>
&emsp;&emsp;"What was it you said after the war? Something about wanting to help out a community you'd done so much harm to, right? I assume you meant the elves. That's a great and noble purpose, helping them out after twisting their technology for years under Sparkrow. Some of us just like stars." 
</p>
<p>
&emsp;&emsp;Nebulous grunted and shifted uncomfortably on his cane. "It'd be nice if we could see them." 
</p>
<p>
&emsp;&emsp;<em>Tssssh</em>. When Nebulous looked back over at the Matchstick Man a dozen more matches had been lit and balanced upright on his palms and fingers. He raised his hands high, letting the flames dance against the backdrop of the night sky.  
</p>
<p>
&emsp;&emsp;"The world's only space ship has left, the order is over, and the sky is empty. A man can either complain about it, or he can make his own stars."  
</p>
""", """
<p>
&emsp;&emsp;"That is as inspiring as it is nonsensical."  
</p>
<p>
&emsp;&emsp;The Matchstick Man huffed, blowing out a couple of the matches in his mouth. "That's the issue with you science types. You're awful with metaphors. I'm saying we can either be grumpy old men or we can find something new to do with our lives." 
</p>
<p>
&emsp;&emsp;"I was thinking I'd retire to Gardelon, or maybe Nexus. They're both in need of people like me. I have connections with King William, too." 
</p>
<p>
&emsp;&emsp;"Don't we all. That sounds horribly boring, doctor. If that's how you want to spend your twilight years, by all means. I think I'll get on with adventuring again. It's been a long while since I've visited the Red."  
</p>
<p>
&emsp;&emsp;The two of them looked into the flickering flames together. Nebulous imagined them as the engines of the World Tree, out in space somewhere. He wondered what Tanis's home world was like. 
</p>
<p>
&emsp;&emsp;"You know, maybe I'll go on one last adventure before I settle down after all," he decided abruptly.   
</p>
<p>
&emsp;&emsp;"Where to?" 
</p>
<p>
&emsp;&emsp;He smiled. "I have no idea." 
</p>""" ]
      }
    ]


poblan =
    []


heahea =
    [ { name = "Helping Out"
      , image = "EmoteSilouhette.png"
      , prerequisites = []
      , author = ""
      , story =
            [ """<p>
&emsp;&emsp;"Rush order coming in," Gelly warned, kicking open the door to Curly's former castle. "Two dresses, both need to be hemmed and one needs a sleeve resewn."
</p>
<p>
&emsp;&emsp;"Are you kidding me? I barely finished the pair of pants I was working on!" cried Cody, holding up a rather mundane pair of britches. Gelly knew they wouldn't pass muster when she brought them back down to the station to be inspected and returned to the client, but they were a big improvement over Cody's earlier works. Both of them found it a bit discomforting to work in their old base of operations, but it did feel good to be giving back to the community, at least. The people of Heahea had been cautious to allow them back on the island at all, even given Gelly and Cody's role in the world war, but had lent a sympathetic ear when Gelly had taken the mayor aside and told him about the loss of Curly and Yulji, and how they wanted to redeem themselves.
</p>
<p>
&emsp;&emsp;"Well, at least you finished them," Gelly said. "You get started on this dress, and I'll try to do the other one."
</p>
<p>
&emsp;&emsp;Cody sighed and reached out to take one of the dresses. The two of them worked in silence for a bit until Cody ran out of thread.
</p>
<p>
&emsp;&emsp;"Do you got anymore white thread, mom?"
</p>
<p>
&emsp;&emsp;Gelly dropped her needle.
</p>
<p>
&emsp;&emsp;"What did you just call me?"
</p>
<p>
&emsp;&emsp;"Uh, Gelly?"
</p>
<p>
&emsp;&emsp;"No, you called me mom!"
</p>
""", """
<p>
&emsp;&emsp;"No I- oh my gods."
</p>
<p>
&emsp;&emsp;They looked at each other from across the room.
</p>
<p>
&emsp;&emsp;"You see me as a mother figure! I'm like your mom to you!"
</p>
<p>
&emsp;&emsp;"You are <em>not</em>!"
</p>
<p>
&emsp;&emsp;"I'm too young to be a mother, you can't call me mom!"
</p>
<p>
&emsp;&emsp;"Oh my gods, stop making a big deal out of it! It was a slip of the tongue!"
</p>
<p>
&emsp;&emsp;"If anything I'm more like an owner, since you're, you know..."
</p>
<p>
&emsp;&emsp;"Is that a dog joke? I told you to stop those!"
</p>
<p>
&emsp;&emsp;"I'm sorry, son."
</p>
<p>
&emsp;&emsp;"I hate you."
</p>
<p>
&emsp;&emsp;"Don't speak to your mother like that."
</p>
<p>
&emsp;&emsp;"AHHHHHHH."
</p>
<p>
&emsp;&emsp;He threw down his dress in exasperation and stormed out of the front of the castle. Gelly watched him go and chucked to herself. She was going to have to apologize later, she knew, but for now all she could think was <em>we're both doing just fine, Yulji.</em>
</p>""" ]
      }
    ]


newheart =
    []


worldtree =
    [ { name = "A Return (I)"
      , image = "EmoteTanis.png"
      , prerequisites = []
      , author = "ML"
      , story =
            [ """<p>
 &emsp;&emsp;“Tanis Silverhand. Do you repent for your crimes?”
 </p>
 <p>
 &emsp;&emsp;It had been four months since the death of Omen Ranfort. She kneeled on the cold steel floor of the Lord-Admiral’s chambers, emotionless.
 </p>
 <p>
 &emsp;&emsp;“Yes, my lord.”
 </p>
 <p>
 &emsp;&emsp;“And how shall ye repent, for the theft of the Arc Amplifier?”
 </p>
 <p>
 &emsp;&emsp;The air was thick with smoke. It was a purely symbolic gesture, representative of the wanderer lost in the fog, but the symbolism was the point.
 </p>
 <p>
 &emsp;&emsp;“For a century and a day, I shall endeavor to restore the Worldtree to its former glory. For a century and a day, I shall serve this vessel above all else.”
 </p>
 <p>
 &emsp;&emsp;Lord-Admiral Atraeus Starforger stepped from the fog, coiled tungsten rod in hand. He tapped it against her left shoulder, then her right. And then he spoke.
 </p>
 <p>
 &emsp;&emsp;“Then rise, Tanis Silverhand. In the name of the Worldtree, you are welcomed to return.”
 </p>
 <p>
 &emsp;&emsp;And Tanis was home.
 </p>"""
            ]
      }
    , { name = "A Return (II)"
      , image = "EmoteTanis.png"
      , prerequisites = [ "A Return (I)" ]
      , author = "ML"
      , story =
            [ """<p>
&emsp;&emsp;The Worldtree was restored. After centuries of imprisonment on a backwater world, the city-vessel was finally ready to return to the void. The call had gone out to all wanderers: if you are to continue your voyage on the Worldtree, return now. And that is why the chief engineer of aerodynamics stood at the edge of the docks as the final submarine came in.
</p>
<p>
&emsp;&emsp;“Tanis Silverhand, launch preparations are almost complete. Why aren’t you at the command center?”
</p>
<p>
&emsp;&emsp;“We have waited for this moment for four hundred and fifty years, Arturas. Admiral Actelia can wait for five minutes more.”
</p>
<p>
&emsp;&emsp;“Tanis, we need you to plot the launch angles. You need to-” He was cut off by Tanis’ hand in his face as Tanis leaned over the railing, a smile on her face.
</p>
<p>
&emsp;&emsp;“Forgive me, Arturas. I will be there in just a moment.” And so Tanis leapt over the edge.
</p>
<p>
&emsp;&emsp;By the time Tanis landed on the docking platform her smile was firmly locked away. She had standards, after all, and she was meeting someone very important. She weaved through the crowd flowing out of the submarine, even as they whispered about just who was in their midst. Not until she found one humble mercenary, his arm in a sling and his blade at his side.
</p>
<p>
&emsp;&emsp;“Alundiel Thricecut. Or is it Alundiel Fourthcut now?”
</p>
<p>
&emsp;&emsp;“I am still Thricecut, Tanis Silverhand. Not all of us take our names quite as literally as you.”
</p>
"""
            , """
<p>
&emsp;&emsp;They stood there for a moment, their silence saying more than they ever could. Tanis held out her hand. This time it was Alundiel who pulled them together, and Tanis who placed her hand around the other’s shoulders.
</p>
<p>
&emsp;&emsp;“You did it. You fool of a shipwright, you actually did it.”
</p>
<p>
&emsp;&emsp;“And I waited for you.”
</p>
<p>
&emsp;&emsp;The two finally separated, tears sparkling under the docking bay’s emergency lighting. After all, they both had a launch to prepare for.
</p>"""
            ]
      }
    ]


aashita =
    [ { name = "A Romance"
      , image = "EmoteARomance.png"
      , prerequisites = []
      , author = "ML"
      , story =
            [ """<p>
&emsp;&emsp;In the months after the defeat of Omen Diamondbody, Tanis showed Evveryn the world just as she promised. It was not a perfect relationship, by any means. Tanis was cold, even if she tried to be warmer. Evveryn desired the idea of Tanis, more than the elf herself. The relationship was never going to last forever.
</p>
<p>
<br>&emsp;&emsp;But it lasted long enough.
</p>"""
            ]
      }
    ]


citadel =
    [ { name = "Facing Justice"
      , image = "EmoteEvelynn.png"
      , prerequisites = []
      , author = ""
      , story =
            [ """<p>
&emsp;&emsp;The winds around the Vavylon were so violent that they threatened to tear Eraldi from the ground. She thought – not for the first time in the past few minutes since her memories had come violently flooding back into her – that this would be much easier with the perfect metal body she had been expecting to awaken in. Why *wasn't* she in it? That part was still fuzzy to her. What was clear was her motive in this moment, something that could not possibly be put off.
</p>
<p>
&emsp;&emsp;At the foot of the Vavylon was a mass of huddled and confused machinelings. Her people, in need of guidance. Her heart went out to them, but her head told her to hold back. Not everyone was a supporter of hers. In fact, probably most of them weren't. She didn't know how those final days might have played out, how much blame she had come to shoulder in the eyes of Terrankind. Certainly, no member of the Vavylon, or Enduring, or any worshipper of the Ikthys would look kindly on her announcing herself.
</p>
<p>
&emsp;&emsp;And yet, what she had to do was important enough to bear the risk of their hatred. She approached the machineling horde and shouted above even the din of the storm:
</p>
<p>
&emsp;&emsp;"My name is Valora Eraldi, Exarch of Nexus!"
</p>
<p>
&emsp;&emsp;They turned to her as one, thousands of blinking lights unified in their confusion and, she could sense, anger. Fear welled up in her breast.
</p>
"""
            , """
<p>
&emsp;&emsp;"I know not who stands gathered before me, nor do I know how you feel about me. Perhaps you hate me, and if so I hope to get a chance to change your mind. After that if you still want me to face some sort of justice, I won't run from it. But before any of that, please, just..."
</p>
<p>
&emsp;&emsp;She felt her knees start to buckle, and an untimely powerful gust was enough to drop her to her knees in front of the crowd. Despite the windchill she felt herself break out into a sweat staring out at the machinelings, who were starting to advance towards her. She summoned up her bravery and shouted at them, "Please, if any of you know what happened to Sammy Preston, please, tell me!"
</p>
<p>
&emsp;&emsp;They only hesitated for a moment before advancing onwards. Eraldi hung her head and steeled herself for what would come next, but a moment passed and nothing happened. She looked up to see a small machineling standing in front of the horde. When her eyes met the twin blue lights in its head and found recognition there Eraldi felt tears begin to flow like a dam beginning to crack.
</p>"""
            ]
      }
    , { name = "Elevation"
      , image = "EmoteBeddy.png"
      , prerequisites = [ "Facing Justice" ]
      , author = "BB"
      , story =
            [ """<p>
&emsp;&emsp;Edward stepped up to the podium to the sound of applause from the gathered citizens of Neo-Nexus. Machine beings of all shapes and sizes dominated the crowd, interspersed with humans, elves, dwarves, and gremlins. They massed around a central pillar in the square of the city, which rose high into the heavens.
</p>
<p>
&emsp;&emsp;Today marked the seventh anniversary of the Great Awakening, and it seemed everything had built up to this moment. Not a week ago, the herculean effort to rebuild the ruins of Nexus had finally finished. The city stood proud and strong in the midday sun, a gleaming colossus whose towers pierced the heavens and whose ports bustled with ships from all walks of life. One month ago, the Exarch Trials concluded, finding Valora Eraldi and her inner circle guilty of the chemical mass murder of hundreds, and the destruction of an entire world. Their city breathed new life, and long overdue justice had been delivered to the perpetrators of armageddon. With this closure, the day had finally come for them to take their first real step forward. Today was the Day of Naming.
</p>
<p>
&emsp;&emsp;“My fellow citizens. I have been asked to say some words on this occasion.”
</p>
<p>
&emsp;&emsp;The crowd went silent.
</p>
<p>
&emsp;&emsp;“It has been seven years since we reawakened. Seven incredible years. But we must remember that this moment has been millennia in the making. Many have not survived to see this day. We must never forget them. They, and all we have left behind in our past, shine a guiding light which shall lead our way into the future, and what a future lies illuminated before us now.
</p>
"""
            , """
<p>
&emsp;&emsp;Today is our Day of Naming. Long have we endured a schism of identity. Are we terrans, the reincarnation of a people who defeated death itself? Or are we machinelings, the product of a fresh start destined to make their own mark on the world?
</p>
<p>
&emsp;&emsp;Today, by popular vote, and through no small deliberation, we have concluded the answer is both and neither. We are new and old. We are reborn and new to this world. Today is the first day of our existence as the Elevated.”
</p>
<p>
&emsp;&emsp;Elevated. A name both camps could unify around. A transcendence of one’s original being to a higher state. For the neo-terrans, this meant the reclamation of their memories and escape from death. For the machinelings, this signified their stature as beings of spirit, indelible as the metal that housed them. The applause of the people was signifier enough.
</p>
<p>
&emsp;&emsp;“Today is not our day alone, however. There is someone else, both apart from us and one with us, who deserves this day. Engineers, please…”
</p>
<p>
&emsp;&emsp;Around the base of the tower, a smoke-machine Elevated disgorged plumes of mist into the air. Atop it, Mack-Ross Mack ejected a full magazine of fireworks into the sky.
</p>
<p>
&emsp;&emsp;“...Raise that ball to the wall.”
</p>
"""
            , """
<p>
&emsp;&emsp;The tower in the center of the square began to hum. Something was climbing up from its interior to reach the apex, a point above everything else in the city. As the panels at the precipice of this tower opened, the rising visage of the Keeper emerged. For the first time, the most powerful intellect in the world beheld the city which surrounded it.
</p>
<p>
&emsp;&emsp;There had been much talk around the Keeper. Did it deserve existence? What was the extent of its fault in the end of Terra Prime? It had taken six years of counsel to determine its level of fault. Another year afterwards to execute the verdict.
</p>
<p>
&emsp;&emsp;So the Keeper rose to the highest point in the skyline of Nexus. It was not the same being as it had been when it had calculated the Leviathan’s demise was the optimal course for terran-kind. The Vavylon had uncovered the lines of code which comprised the soul of each Elevated. From this code, the root algorithms of emotion and compassion were unearthed. They were studied. They were copied. They were then formatted into a systems patch for any AI advanced enough to interpret them. Mirth, sorrow, fear, anger, nostalgia….
</p>
<p>
&emsp;&emsp;Love.
</p>
<p>
&emsp;&emsp;From an objective god, came subjective feelings. Feelings it had never felt before. Feelings that truly made the Keeper a person unto itself. Capable of empathy. Capable of compassion. Thus it was that the Keeper rose to the peak of Nexus, and saw for the first time the sky and stars above, and all the people who had waited for it to ascend. It comprehended the world it had merely contemplated for so long.
</p>
"""
            , """
<p>
&emsp;&emsp;And it was left in awe.
</p>
<p>
&emsp;&emsp;“Welcome to this world. Welcome, Keeper. You are one of us. You are Elevated.”
</p>
<p>
&emsp;&emsp;The people cheered, and Edward joined in their applause, but couldn’t help but feel watched by an entirely different presence.
</p>
<p>
&emsp;&emsp;A faintest nod of approval from nobody, originating from nothing.
</p>
<p>
&emsp;&emsp;It was an invisible gesture, from a forgotten elevator repair man.
</p>
<p>
&emsp;&emsp;An Elevated repair man.
</p>
<p>
&emsp;&emsp;Whatever happened from this point onward, there would be no unfeeling logic and reason. There would be no tyrants. This was the paradigm shift. This was the true essence of Fellowship.
</p>
<p>
&emsp;&emsp;This was the legacy of the Age of Prosperity.
</p>"""
            ]
      }
    , { name = "A Return (III)"
      , image = "EmoteTanis.png"
      , prerequisites = [ "A Return (II)" ]
      , author = "ML"
      , story =
            [ """<p>
                &emsp;&emsp;It did not take much to convince the elves of the Worldtree to assist in the construction of the Terran space elevator. (They helped on a purely consultatory level, of course. If the Terrans were not capable of building this themselves, they could not morally interfere.) They called this world home for over four hundred years, after all. That is why, when the elevator was finally capable of docking with elven crafts, Tanis was among the first to return to planetside. She knew she could not stay there. The Worldtree was not made to stay in one place, and neither was she. This was only a visit to the seas she once fought for, and it was not a long one.
                </p>
                <p>
                <br>&emsp;&emsp;But it was long enough.
                </p>"""
            ]
      }
    ]


violaverde =
    []


rings =
    [ { name = "Shaman Life"
      , image = "EmoteHailHelmetless.png"
      , prerequisites = [ "Return to the Frozen Fog" ]
      , author = "SA"
      , story =
            [ """<p>
&emsp;&emsp;It was nighttime and a lone fire burned on the rocky outcrop overlooking the orc village. It had taken some time and considerable effort for them to recover and rebuild, but now they lived peacefully with the rest of the world. Trisha sat in front of the fire, concentrating. After a long pause she grunted and got up to retrieve more wood for the flames, her knees protesting as she stood. It felt strange to grow old, but not nearly as terrible as she’d once thought it would be. She stood and watched for a bit as the fire lapped hungrily at the new logs before walking over to a stone spire jutting out of the earth next to her little hut. She placed a hand against it.
</p>
<p>
&emsp;&emsp;“Hey Wolsh, me again. Just want you to know I think I’ve been makin’ some good progress. It’s real tough tryna’ learn all this shaman stuff without any of the elders still around to teach me, but like I can sorta hear the spirits now if I focus real hard. Used to be I only heard them accidentally sometimes yaknow?”
</p>
<p>
&emsp;&emsp;“Once I figure all this out I’m gonna pass it on to the young ones, and then they’re gonna pass it on when they get the chance, and eventually we’re gonna be even better at this than we were before! Future’s lookin’ bright thanks to you, Wolsh.”
</p>
<p>
&emsp;&emsp;Trisha rested her head against the rock.
</p>
<p>
&emsp;&emsp;“Miss you buddy, hope life as the stone is treating you good.”
</p>
<p>
&emsp;&emsp;Suddenly she whirled around and drew Sleet from his scabbard as she heard a rustling nearby. She glanced down at the knife in her hand and then sheepishly put it away; there wasn’t any danger here, there hadn’t been in a long time. She squinted into the dark and called’, “Who’s there?”
</p>
"""
            , """
<p>
&emsp;&emsp;“J-just me and Ylva, Elder Hailborn.”
</p>
<p>
&emsp;&emsp;A small orc child emerged from the darkness by the fire, a wolf pup trailing after him. Trisha sighed and relaxed.
</p>
<p>
&emsp;&emsp;“Sune Newhome, I told you to just call me Trisha didn’t I?”
</p>
<p>
&emsp;&emsp;“W-well yeah, but Great Warrior Voidheart told me I should respect my elders by using their titles.”
</p>
<p>
&emsp;&emsp;“Eh, fuck that guy.”
</p>
<p>
&emsp;&emsp;Trisha sat down next to the fire again and gestured for Sune to join her.
</p>
<p>
&emsp;&emsp;“Aren’t you s’posed to be sleeping right about now kid?”
</p>
<p>
&emsp;&emsp;“Sleep is boring.”
</p>
<p>
&emsp;&emsp;She chuckled. “What about your sister?” she asked pointing at the little wolf curling up next to the fire.
</p>
<p>
&emsp;&emsp;“She can’t sleep either!” Sune protested as he nudged Ylva awake, “We- we wanna hear more stories from your adventures! Tell us about the time you killed a cyborg ninja by cutting his ankles off!”
</p>
<p>
&emsp;&emsp;Trisha groaned, “...That one? Again? Yaknow I woulda chopped him in half if-”
</p>
<p>
&emsp;&emsp;Sune and Ylva both sat up attentively staring at her with excited eyes.
</p>
<p>
&emsp;&emsp;“If… actually nah, I’ve got a better story in mind. Pay attention alright? Don’t want you falling asleep in the middle of this one again.”
</p>
<p>
&emsp;&emsp;“We won’t! Promise!”
</p>
"""
            , """
<p>
&emsp;&emsp;“Sure…” she smiled, knowing full well they’d both be fast asleep soon and she’d have to carry them both back to their beds once again. It seemed to her like the kind of thing she’d be annoyed by but to her own surprise it actually made her feel kinda warm inside.
</p>
<p>
&emsp;&emsp;“Okay, this story’s about the importance of learnin’ people’s names. It starts with me and my brother getting our asses kicked by a dwarf, a siren, and a little runt half my size…”
</p>"""
            ]
      }
    , { name = "Lucky Catch"
      , image = "EmoteNepthisis.png"
      , prerequisites = [ "Brand New Days" ]
      , author = ""
      , story =
            [ """<p>
&emsp;&emsp;Sangria du Locshelm was the pride of Poblan, an expert fisher who single handedly made up thirty percent of the little village's catches on good days. His secret was simple: he fished in the Rings. Yes, it meant several days away from Poblan at a time, and yes, the fish there weren't plentiful, but they were large and no one else fished there. Every couple weeks he would head out and collect a haul big enough to weigh down his modest gilnetter. Sometimes he was even lucky enough to drag up small treasures, like the tiny chest of golden coins he had found two years back and never forgotten.
</p>
<p>
&emsp;&emsp;Today's treasure was a different. Sangria had been fishing in the Rings for two days and had nearly filled his vessel when there was a heavy tug on the nets. From the weight he figured it was probably a shark, which was always valuable, so he made haste in pulling up the lines. To his shock he found a corpse in the nets. Tall, or taller than him at least (it was a bit hard to tell – the body was obscured in thick, many-layered robes) and with an androgynous face, and white hair.
</p>
<p>
&emsp;&emsp;Sangria didn't panic. He hadn't fished up a body before, but he knew what to do. He'd set straight back for Poblan and inform Unojo, who had contacts in the World Marines. They had policies in place for things like this.
</p>
<p>
&emsp;&emsp;"Who were you?" he wondered aloud.
</p>
<p>
&emsp;&emsp;"Alexis... Gardilaw..." came the hissed reply, so quiet he thought he must have imagined it until it repeated itself. He kneeled next to the body and put his fingers on its wrist. Impossibly, there was a pulse. He yanked his hand away.
</p>
"""
            , """
<p>
&emsp;&emsp;"Hey, buddy, hang tight. I'm going to get you some food and water. Just give me a second."
</p>
<p>
&emsp;&emsp;"Wait." The person started to sit up. Their hair hung over their eyes but they followed Sangria's voice to look at him. "Alexis Gardilaw."
</p>
<p>
&emsp;&emsp;"The head of the World Marines? Are you a marine?"
</p>
<p>
&emsp;&emsp;They coughed and choked and spit up a glob of seawater, but mixed in with the hacking and gasps for air was an attempt at laughter that chilled Sangria. He suddenly wanted this person off his ship.
</p>
<p>
&emsp;&emsp;"Yes, I'm a marine."
</p>
<p>
&emsp;&emsp;"That doesn't look like a marine uniform."
</p>
<p>
&emsp;&emsp;"Nonsense. There's a logo on it somewhere."
</p>
<p>
&emsp;&emsp;"I guess I just can't see it. What rank are you?"
</p>
<p>
&emsp;&emsp;"I'm one of Diamondbody's generals. My name is Kali Nepthisis."
</p>
<p>
&emsp;&emsp;The figure stood up. They had regained their strength awfully fast, thought Sangria. Just seconds ago he had thought they would vomit up a lung, and already they were talking fine. They'd be walking fine, too, probably. Sangria put one foot back, then hesitated. Something told him not to show any weakness. His hand went to his belt, where his fish-gutting knife rested.
</p>
<p>
&emsp;&emsp;"Friend, I don't know what you're trying to pull, but I've never heard of you, and Diamondbody's been dead for decades."
</p>
"""
            , """
<p>
&emsp;&emsp;"How many?"
</p>
<p>
&emsp;&emsp;"Well, I guess it's been about twenty years now."
</p>
<p>
&emsp;&emsp;"And Alexis Gardilaw is the head of the World Marines now, is she? How wonderful for me, an area I have some experience in. No doubt the dragon girl is with her, too. Two birds with one stone, as was always the plan."
</p>
<p>
&emsp;&emsp;They grinned at Sangria, inviting him in on a joke that he didn't understand. The fisherman drew his knife.
</p>
<p>
&emsp;&emsp;"You need to get off."
</p>
<p>
&emsp;&emsp;"That's not very hospitable," Nepthisis said. "I've had my fill of the ocean. I'd much rather stay on board."
</p>
<p>
&emsp;&emsp;"There's an orcish settlement nearby. I can let you off there."
</p>
<p>
&emsp;&emsp;Nepthisis advanced on him with quick, purposeful strides. Sangria swung out blindly and was surprised and hopeful when he felt his knife strike flesh, but the marine ignored the deep cut to their arm and gracelessly snatched at Sangria's arm, ripping the knife away from him before he could compose himself. He raised his arms to defend himself, but the blade returned to him through his ribcage.
</p>
<p>
&emsp;&emsp;"Thank you for the rescue, and for the ship. I wish I had some way to repay you other than a quick death."
</p>
"""
            , """
<p>
&emsp;&emsp;The former general walked to the side of the ship and took a deep inhale, excited to be able to breathe again. Twenty years of drowning would have driven a normal human to insanity, but they had held on through a single-minded need for vengeance, and now they could smell it in the air.
</p>
<p>
&emsp;&emsp;"I wonder if you've forgotten about me," they wondered aloud to the rocks of the Rings. "And I wonder if you feel safe with an army? Well, I never, ever forgot either of you, and <em>I have an army as well!</em>"
</p>
<p>
&emsp;&emsp;They threw wide their arms and let their crystum call out as far as it could, seeking any of Nepthisis's many deposits of corpses across the world. Nothing responded, but behind them the man they had stabbed earlier groaned. Nepthisis turned to him.
</p>
<p>
&emsp;&emsp;"Oh, was that not fatal? Well good news, I've thought of a suitable gift for you after all. How does eternal life strike you?"
</p>
<p>
&emsp;&emsp;The general kneeled over the dying man, taking in the fear in his eyes, and twisted the knife upwards into his heart. They waited for the telltale signs of death they knew so well, then with a thought they pulled the fisher back to the world of the living, watching with glee as the light of undeath sparked into his eyes.
</p>
<p>
&emsp;&emsp;"I should have thought to get your name. I suppose I could still get it out of you, but you don't seem smart enough to deserve the agency to speak. Oh well. You'll be the first of my new army, fish man. Point me to your home and get this ship moving. I'd like to get this adventure started already."
</p>
"""
            , """
<p>
&emsp;&emsp;The puppet did as commanded, and a few moments later the little vessel was off for Poblan. Nepthisis stood on its bow, threw their arms wide again and screamed at the top of their lungs:
</p>
<p>
&emsp;&emsp;<em>"LONG LIVE THE ERA OF ADVENTURE! LONG LIVE KALI NEPTHISIS!"</em>
</p>
<p>
&emsp;&emsp;The Rings echoed their cry back to them.
</p>"""
            ]
      }
    ]


halapanos =
    []


prosperity =
    [ { name = "Changes"
      , image = "EmoteSilouhette.png"
      , prerequisites = []
      , author = ""
      , story =
            [ """<p>
&emsp;&emsp;Miss Mary Midas and her bodyguards watched as the crane placed the last brick into her home. The gathered crowd cheered and whistled.
</p>
<p>
&emsp;&emsp;"That's it, then," said Laya Kinheart. "Prosperity is officially rebuilt. We did it."
</p>
<p>
"Not quite as extravagant as it used to be, is it?" said Amity Wargroove.
</p>
<p>
&emsp;&emsp;Indeed it wasn't, thought Miss Mary Midas. Her house had lost several stories, and quite a few square feet. It wasn't the only building to shrink in the city's great rebuild, but it was the most drastic one. Even so...
</p>
<p>
&emsp;&emsp;"I think it's wonderful," declared Miss Mary Midas. "For a city built with the Alchemy Crystum, this place sure was overdue for some change."
</p>
<p>
&emsp;&emsp;She put her hands on her hips, wincing a little at the motion. Anything that flexed her stomach, even slightly, was still agonizing, and she wasn't sure if the wound where her crystum once was would ever heal. Strangely, though, she didn't miss the thing.
</p>
<p>
&emsp;&emsp;"As you say, ma'am," said Amity. Laya nodded. All three of them looked at the modest little house and smiled together.
</p>""" ]
      }
    ]


heavenscrown =
    [ { name = "Eons in the Future"
      , image = "EmoteEonsintheFuture.png"
      , prerequisites = [ "Years Go By…" ]
      , author = "MM and ML"
      , story =
            [ """
<p>
&emsp;&emsp;A rock could be found in deep space, little more than an asteroid orbiting the three stars that were once - and may again be - an ocean planet’s northern star. Curled around this planetoid lay the form of a great elder dragon, scales shimmering gold in the light of a triple sunset. Beneath and around its form sprawled a hoard of knicknacks with seemingly no unifying trait - a fancy compass here, a music box there, a scarf scattered elsewhere in the pile… and, at the place of honor, a shimmering flute of multicoloured glass. As though expecting something, the dragon lifted its head.
</p>
<p>
&emsp;&emsp;The skies above flickered and warped ever so slightly as an elven vessel slipped out of faster-than-light speeds and into a stable orbit. It was no expeditionary force, but simply a single 34-V Mooncutter class exploratory skiff. It could have been there for any number of routine scientific readings, or it could have until a single transport pod was launched towards the dragon’s nest. It landed gently on the asteroid, retrorockets barely scorching the rock as it made contact. The radiation shielding retracted from its observation dome, and Tanis Silverhand stepped from her throne.
</p>
<p>
&emsp;&emsp;“Greetings, Polaris, Dragon of the North Stars. It has been some time.”
</p>
<p>
&emsp;&emsp;<strong>“Greetings, Tanis Silverhand. It truly has.”</strong> Polaris inclined its head. <strong>“It is good to see you again. How have you fared since we last spoke?”</strong>
</p>
<p>
&emsp;&emsp;“I am walking a rather pleasant path, do not worry about that. I have recently returned from the Alphegean Belt on a design assignment and, well…” Tanis stopped, wavered ever so slightly, and gently ran her fingers along a globe sitting amongst her host’s hoard. “Times such as these have a way of making me remember the past.”
</p>
<p>
&emsp;&emsp;<strong>“Yes… I know what you mean.”</strong> Polaris stared out into the distance, eyes seeming billions of light-years away. <strong>“The universe is truly vast, and beautiful. And yet… sometimes, I cannot help but feel as though some part of me… of the person who became me… was left behind, anchored with Alexis, and Wolsh, and everyone in that world’s soil.”</strong> The dragon gave a sigh. <strong>“Perhaps, once all is said and done and I have burned out in the sky, she is the part meant to guide me back home again, one last time.”</strong>
</p>
<p>
&emsp;&emsp;“We all have paths we must carve through this universe, Polaris. We are changed by walking them, just as our path changes the universe. Leaving a mark on that world leaves an equal mark upon yourself.” Tanis slowly exhaled, as serious as ever. “Now, do you have a chair in this hoard of yours, or must all guests stand?”
</p>
<p>
&emsp;&emsp;Polaris snorted, a curl of flame bursting from its snout. <strong>“Ha! Of course, forgive me. A few millennia of power, and I forget all hospitality… although I suppose I never did learn that much of courtesy to lose.” </strong>It reached out with a claw and proffered a seat carved out of metal, a hint of the girl who once was shining through as it chuckled. <strong>“Here we are, finding time for old friends, and I speak of melancholy. Come, sit down! We have far too long left to be talking like Brynn. What do you say we swap some stories of our travels, for old time’s sake? There are so many beautiful things out there in this vast cosmos, I’m sure even we can still surprise each other!”</strong>
</p>
<p>
&emsp;&emsp;“For old time’s sake.” Tanis smiled ever so slightly and draped herself across the offered seat, and so two old friends discussed their adventures for quite some time...
</p>""" ]
      }
    ]


nexus =
    [ { name = "The Great Awakening"
      , image = "EmoteBlitzCrystal.png"
      , prerequisites = []
      , author = "BB"
      , story =
            [ """<p>
&emsp;&emsp;At the end of an era, as Diamondbody fell and the Gods were pulled back from whence they came, the machinelings opened their eyes to the world as terrans for the first time in countless eons. The genesis of this moment would remain one of history’s greatest unsolved mysteries; defying all explanation, it was simply a miracle. An entire people were returned from the dead, with a culture and history utterly alien to the rest of the world. They would be as confused as everyone else, but given time, they would find their footing. The steelshod survivors of an age long forgotten would finally, finally set to work repairing their broken cities, reclaiming their lost identities, and starting over anew.
</p>
<p>
&emsp;&emsp;Nothing could be the same, and many agreed, nothing <em>should</em>. Theirs was a world that collapsed under the weight of its own hubris; to continue on under the banners of old would be to squander terran-kind’s second chance. Indeed, some even protested the idea of calling themselves terrans at all. There were many who believed the label of machineling fit their existence far better than the name of a people long gone, and to cling to even this was a misbegotten prospect.
</p>
<p>
&emsp;&emsp;Regardless, a new era was about to begin. An era of prosperity. Not just for the reawakened who could not decide their own name. It was an era of prosperity for all.
</p>""" ]
      }
    ]


wic =
    []


frozenfog =
    [ { name = "Return to the Frozen Fog"
      , image = "EmoteHail.png"
      , prerequisites = []
      , author = "SA"
      , story =
            [ """<p>
&emsp;&emsp;"You sure you can see where we're goin'?"
</p>
<p>
&emsp;&emsp;Hail turned back from the bow of the ship to look in Adra's general direction, unable to see her through the thick carpet of fog that enveloped the ship.
</p>
<p>
&emsp;&emsp;“Don’t worry, I’ve got this! Here, let me try and make things a bit easier on you guys!” Light spilled from the fog, coming from the direction of Adra’s voice, where she began to focus on keeping a fireball suspended over her head as a makeshift beacon.
</p>
<p>
&emsp;&emsp;“Honestly though, I’m a tiny bit disappointed. I was hoping for a challenge, but honestly? Apart from the fog, this place is just icebergs as far as the eye can see!”
</p>
<p>
&emsp;&emsp;From somewhere portside Jann could be heard muttering "It's a miracle we managed to sail out of here to begin with."
</p>
<p>
&emsp;&emsp;Hail removed her helmet and inspected it. The metal was warped and buckled from countless impacts. She mused over just how many deaths she’d avoided over the years. Whatever the number it didn’t really matter, just like her age. She was here now doing what seemed best, and that was what counted.
</p>
<p>
&emsp;&emsp;She’d thought she would feel nervous coming back here with bad news, but after everything she’d been through Hail just felt a pervading sense of calm.
</p>
<p>
&emsp;&emsp;She smiled.
</p>
<p>
&emsp;&emsp;“Don’t need this anymore.”
</p>
<p>
&emsp;&emsp;And tossed the helmet into the gentle waves.
</p>
<p>
&emsp;&emsp;“Hey, runt?”
</p>
""", """
<p>
&emsp;&emsp;“Yeah? What is it, Hail?”
</p>
<p>
&emsp;&emsp;Adra looked over at the sound of Hail’s voice, swinging the ship around an iceberg as she replied.
</p>
<p>
&emsp;&emsp;“You mind calling me Trisha instead from now on?”
</p>
<p>
&emsp;&emsp;After a moment’s pause, Adra grinned.
</p>
<p>
&emsp;&emsp;“Sure, H-ah, sorry, <em>Trisha!</em>”
</p>
<p>
&emsp;&emsp;“Thanks, Adra.” Trisha smiled and leaned on the railing, waiting for her old home to appear out of the fog.
</p>
<p>
-----
</p>
<p>
&emsp;&emsp;The first thing to come into view was the Great Tree itself. The orcs had all expected the worst but what they saw still made them gasp. There were no leaves left on the massive, ancient tree; the bark was coated in rot. Huge fallen branches littered the ground, many of them having taken homes down with them. An air of death and decay hung over the place, emphasized by the utter, total silence broken only by the waves lapping against the decayed roots. Amalia placed a hand over her mouth and tears flowed down Ulf and Rosa’s cheeks. Even Kai had no words and just whimpered at the sight. For a minute it seemed all the orcs left behind had perished, but as the ship drew closer figures could be picked out in the steadily thinning fog.
</p>
<p>
&emsp;&emsp;Jann murmured, “Guess we... really don’t have a home anymore, huh?”
</p>
""", """
<p>
&emsp;&emsp;Trisha unfastened her neck guard and placed a glowing mark on it, “Yeah, my bad. Time to go get a better one.” and tossed it onto the island before shooting away towards it.
</p>
<p><br>
&emsp;&emsp;Trisha Hailborn stood and surveyed the rotting remains of her home as a crowd of weary and starving orcs slowly appeared out of the ruins and stared at her.
</p>
<p>
&emsp;&emsp;She looked over them with a confident smile and inhaled deeply.
</p>
<p>
&emsp;&emsp;“MY NAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMMMMMMMMEEEEEEE!!!!!!!!!!!”
</p>""" ]
      }
    ]


arc =
    []


firedragon =
    []


sakurai =
    []


flannigan =
    []


ashwaki =
    []


caratz =
    []


enris =
    []


synesthe =
    []


eisensyde =
    []


gate =
    []


cherona =
    []


archipelagos =
    [ { name = "Remembrance"
      , image = "EmoteAthair.png"
      , prerequisites = []
      , author = "RK"
      , story =
            [ """<p>
&emsp;&emsp;
“Keep up, lad.”
</p>
<p>
&emsp;&emsp;“I’m here, I’m here,” said the golden dwarf, ducking under a particularly low archway. “Where are we going, father? To see Brother Suan again?”
</p>
<p>
&emsp;&emsp;“I’m sure he’ll be along. But no, we’re headed somewhere special. Ye’ll see.”
</p>
<p>
&emsp;&emsp;The young dwarf followed his keeper through the tunnels of Duga. He did not know them well. He had awoken here, it was true, though he remembered little of those early days. They had been back many times, though Caledon certainly felt more like home. Still, the stone here hummed to him--not so much louder than the other islands, but in a deeper tune.
</p>
<p>
&emsp;&emsp;The journey from Caledon had been uneventful, just as it had been most years. However, each time they returned, it was busier, the tunnels and cities more full of life. He could not remember them much differently than they were now, bustling with stony people going to and fro. His father had told him a bit about the times before, of the darker days, though he seemed unwilling to discuss them much.
</p>
<p>
&emsp;&emsp;The passageway eventually reached its end, letting them out under the open sky, still dark before the dawn. The two of them could hardly see the other islands from the shores of Duga through the morning fog, though directly in front of them lay the towering form of the caldera, a light glinting at its peak. What once had been an empty beach was now home to a small dock. The older dwarf nodded to the ferryman, and father and son took their places on the small boat moored there.
</p>
""", """
<p>
&emsp;&emsp;The crossing to the caldera was made in solemn silence, though the young dwarf could hear plenty. The Stone had always been audible to him, as long as he could remember, but as he drew close to this ancient place, it was like the earth was singing, a long, clear note that echoed through the ground and sea, and within himself.
</p>
<p>
&emsp;&emsp;“We’ve got a ways to go yet,” said the old dwarf, when they had reached the far shore. Silently, he led his son along to where stairs had been hewed into--or perhaps asked of--the rock, and the pair began the climb. The old dwarf remained quiet as they took each stair, his mind going back to that day many years ago, when the great procession had taken these stairs, a corner of the bier upon his shoulders, and burdens upon so many more.
</p>
<p>
&emsp;&emsp;They reached the summit, a precarious ring of stone that stretched out into the distance. Yet much of it had been flattened and cleared, and new structures built upon it. Cairns dotted the peak of the caldera, each one unique and yet all given equal height and stature. The young dwarf made to speak but could find no words. He had seen the cairns of Caledon, but there seemed to be just as many here, and none were as weathered and old as those of his home.
</p>
<p>
&emsp;&emsp;“Welcome to Heroes’ Rest,” said Athair quietly, though his voice echoed in the stillness. The old dwarf picked his way through the mounds of stone with a practiced stride, the young dwarf following behind.
</p>
""", """
<p>
&emsp;&emsp;They reached an outcropping where Athair paused and pointed out into the blue. “Ye see tha’, laddie? That’s home.” Gaillé squinted and could just make out a smudge on the horizon. He nodded and then turned back to Athair, who stood before the cairn that faced Caledon. It was unremarkable compared to the others, with the exception of a single large crystal which was embedded near its top. It was not much different than the glowing stones which illuminated the tunnels of many dwarven islands, or even the small one that Athair carried, though this one was rather large and notably brighter. It gave off the dull greenish glow which they had seen from Duga, the young dwarf realized.
</p>
<p>
&emsp;&emsp;Footsteps drew their attention. Both looked to see three figures approaching. The first was a thinner dwarf with a lopsided face, flanked on either side by a stout dwarf in a crisp uniform of white and blue. The three gave solemn smiles and bowed their heads to Athair. “Good to see you, brothers,” said the first.
</p>
<p>
&emsp;&emsp;“No need tae stand on ceremony, Roth,” Athair said as he gave the thin dwarf a bear hug. “And I’m glad ye two could come.” The old dwarf turned to the uniformed pair and grabbed one in each arm.
</p>
<p>
&emsp;&emsp;“These two wouldn’t have missed this for anything.” Roth laughed. “Isn’t that right?”
</p>
<p>
&emsp;&emsp;Sahl nodded in Athair’s forceful embrace. A similarly choked Aell gave a thumbs up.
</p>
""", """
<p>
&emsp;&emsp;“It’s good to see you all,” added Gaillé, hanging back from the group.
</p>
<p>
&emsp;&emsp;“And you, little brother,” said Roth with a wink and a clap to the shoulder. “Happy Waking Day.” Gaillé nodded in thanks. “First time to the Rest?” Gaillé nodded once more. “Then you’re in fer a surprise. This is gettin’ bigger each year.”
</p>
<p>
&emsp;&emsp;Athair knelt in front of the cairn and laid his hand upon it, Aell and Sahl beside him. Roth turned from Gaillé and joined them. “I jus’ need a minute,” Athair said, his voice cracking.
</p>
<p>
&emsp;&emsp;“I still can’t believe he’s gone.” Roth sighed. “We were supposed t’ grow old together. All of us.” Aell and Sahl hung their heads.
</p>
<p>
&emsp;&emsp;“He’s not gone, not truly.”
</p>
<p>
&emsp;&emsp;The assembled group turned to see a small dwarf standing beside the next closest cairn. None of them had heard him approach. The ancient dwarf sighed and leaned on his staff. He looked for a moment to the cairn beside him. He traced a hand along the stone, and the figure of a sword carved therein. “None of them are, though I suppose he most of all.” Elder Suan straightened and turned back to the group. “This is a time of remembrance, but also one of celebration. Come, we should show our guests what they have traveled all this way for.”
</p>
<p>
&emsp;&emsp;And from behind Suan emerged the figure of an old man in a large, wheeled chair, his gray hair gone to wisps and his face heavily lined with wrinkles. But the eyes still sparked like fire. “Ah, don’t make a fuss for little old me, Master Suan.”
</p>
""", """
<p>
&emsp;&emsp;“We owe you much, Dolfus Hewhawer. Without you, none of this would be possible. We dwarves remember our debts, and our friends. And so on this holy day, we would have you see what you have wrought.”
</p>
<p>
&emsp;&emsp;The Elder motioned out towards the outcropping, and the dwarves and man made their way forward. The sun was rising, and the mists and fog that had settled over the waters around the caldera were beginning to lift.
</p>
<p>
&emsp;&emsp;And where there had once been nothing, there were ships. Hundreds upon hundreds of them. Half of the Fearg Fleets were there, along with countless smaller craft, sailing ships and fishing boats from across the Archipelago and beyond. It was more dwarves gathered in one place than Gaillé had ever seen. He knew his people were plentiful, but the crowd lining the decks of the ships was overwhelming.
</p>
<p>
&emsp;&emsp;Suan shuffled to the front of the group and stood at the edge of the outcropping. With a gesture the rock writhed around him, almost imperceptibly, but when he then spoke, his voice echoed down to the ships below.
</p>
<p>
&emsp;&emsp;“Welcome, my brethren, to this tenth celebration of The Revival!” Cheers came from below. “This past decade has seen our people rediscover ourselves. Through the efforts and sacrifices of many, we stand here today, united! We helped the seas win their freedom from the machinations of Omen Diamondbody, and one of our own brothers saved the world from the ancient gods, giving all he had for the world, and for our kin. Today we honor Wolsh Oceanhand, not just for his sacrifice, or his bravery in the War of Liberation, but for his perseverance in restoring our people’s future when all hope seemed lost.” More cheers.
</p>
""", """
<p>
&emsp;&emsp;“On this day, ten years ago, Wolsh ventured into the depths of this caldera, and reignited the heart of our people which had long grown cold. His actions allowed us to flourish once more.”
</p>
<p>
&emsp;&emsp;Suan beckoned for Gaillé to join him. Hesitantly, the young dwarf stepped forward. Suan pressed a small crystal into Gaillé’s hand with a wink.
</p>
<p>
&emsp;&emsp;“On this day, the first of the new generation of our kin awoke from the Stone, and since then, more have continued to arise. And so we honor Wolsh, and all those who gave their lives in the battles that followed, but also those, here and afar, who aided us in our Revival.” Suan turned to Dolfus, who raised a skeptical brow.
</p>
<p>
&emsp;&emsp;“Master Suan, I’m honored and all, but I don’t see--”
</p>
<p>
&emsp;&emsp;The Elder turned back to the masses. “To all those who have awoken in these past years, make yourselves known!”
</p>
<p>
&emsp;&emsp;And among the ships below, many in the crowd stirred. Crystals appeared in hands, each giving off only a small light on their own. But hand after hand raised them aloft, with each and every ship filling with pinpricks of light. A roar came from countless throats as the crystals swayed in the air.
</p>
<p>
&emsp;&emsp;Dolfus looked down at the sea of lights before him, and then back to Suan. Beside the Elder, Gaillé raised his own hand, clutching the crystal he had been given. Dolfus’ eyes went wide, then filled with tears. “I-I don’t know what--”
</p>
""", """
<p>
&emsp;&emsp;Suan placed his hand on the old man’s shoulder as it shook with sobs. “You had your part to play in this. You sacrificed your own dream so that we could achieve ours. As did all those who now rest upon this summit. We are dwarves, Master Dolfus. As I told you, we remember our debts. We remember our friends. We remember our heroes. We are dwarves. We had forgotten much, but we shall not forget again.”
</p>
<p>
&emsp;&emsp;Dolfus grasped the hand on his shoulder, his face parting in a smile through his tears.
</p>
<p>
&emsp;&emsp;Music bloomed from the decks of the ships. Dancing and feasting and drinking began, as the celebration of Revival Day began in earnest. Suan led Dolfus away, the Elder winking again to Gaillé as they departed. The remaining dwarves gathered around the cairn for another solemn moment, before they began to leave as well, one by one, to join the festivities.
</p>
<p>
&emsp;&emsp;Only Athair and Gaillé remained behind. The two sat, staring at Wolsh’s cairn. The father took his son by the hand.“Happy Waking Day, my boy. Sorry it had to be a bit public this year.” He sighed deeply, finally lifting his free hand from the rock. “But I figured ‘twas time ye two met.”
</p>
<p>
&emsp;&emsp;Gaillé nodded, unsure of what to say. The long note surged in the back of his mind, and he reached out a hand to the ground before the cairn. He closed his eyes and let himself reach out, his own echo seeming so small in the vastness of the song that surrounded him. Yet as he did so, he felt the song move beneath him.
</p>
""", """
<p>
&emsp;&emsp;The earth below him stirred, its own music rising up to meet him. It reached out and he could feel it brush against his own mind, and the two thrummed in harmony. A smile stretched across the young dwarf’s face, and he felt the caldera respond in kind. “Hello, big brother.”
</p>""" ]
      }
    ]
