import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';

let saveData = localStorage.getItem('saveData');
if (saveData == null) { saveData = ""; }
var d = new Date();
var time = d.getTime();

let elmApp = Elm.Main.init({
  node: document.getElementById('root'),
  flags: {
    time: time,
    width: window.innerWidth,
    height: window.innerHeight,
    saveData: saveData
  }
});

elmApp.ports.sendToJS.subscribe(data => {
    localStorage.setItem('saveData', JSON.stringify(data));
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
