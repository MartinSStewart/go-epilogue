port module Main exposing (..)

import Browser
import Browser.Events
import Element exposing (Element, fill)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Epilogues exposing (Epilogue)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events.Extra.Mouse as Mouse
import Html.Events.Extra.Wheel as Wheel
import Html.Parser
import Html.Parser.Util
import Json.Decode
import Json.Encode
import Pixels exposing (Pixels)
import Point2d exposing (Point2d)
import Quantity exposing (Quantity(..))
import Rectangle2d exposing (Rectangle2d)
import Set
import Time
import Vector2d



---- MODEL ----


type alias Model =
    { time : Time.Posix
    , animationStartTime : Maybe Time.Posix
    , windowWidth : Int
    , windowHeight : Int
    , dragScreenStart : Maybe (Point2d Pixels ScreenCoordinate)
    , mouseScreenPosition : Point2d Pixels ScreenCoordinate
    , cameraWorldPosition : Point2d Pixels WorldCoordinate
    , cameraZoom : Float
    , zoomPoint : Point2d Pixels WorldCoordinate
    , currentIsland : Maybe Island
    , currentEpilogue : Maybe Epilogue
    , currentPage : Int
    , readEpilogues : List String
    }


{-| The coordinate system for our screen. We have "Never" added to the constructor to ensure that this type is never created.
Its only purpose is to annotate Point2d and Vector2d with the coordinate system they belong too.
-}
type ScreenCoordinate
    = ScreenCoordinate Never


{-| The coordinate system for our world. We have "Never" added to the constructor to ensure that this type is never created.
Its only purpose is to annotate Point2d and Vector2d with the coordinate system they belong too.
-}
type WorldCoordinate
    = WorldCoordinate Never


type alias Flags =
    { time : Int, width : Int, height : Int, saveData : String }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { time = Time.millisToPosix flags.time
      , animationStartTime = Nothing
      , windowWidth = flags.width
      , windowHeight = flags.height
      , dragScreenStart = Nothing
      , mouseScreenPosition = Point2d.pixels 0 0
      , cameraWorldPosition = Point2d.pixels 0 0
      , cameraZoom = calculateMinZoom flags.width flags.height
      , zoomPoint = Point2d.pixels 0 0
      , currentIsland = Nothing
      , currentEpilogue = Nothing
      , currentPage = 0
      , readEpilogues =
            case decodeData flags.saveData of
                Ok data ->
                    data

                Err _ ->
                    []
      }
    , Cmd.none
    )


calculateMinZoom : Int -> Int -> Float
calculateMinZoom width height =
    max (toFloat height / 4000) (toFloat width / 6000)



---- UPDATE ----


type Msg
    = NoOp
    | UserPressedIsland Island
    | UserPressedMenuEpilogueButton Epilogue
    | UserPressedMenuCloseButton
    | UserPressedMenuBackButton
    | UserPressedMenuForwardButton
    | OnWindowResize Int Int
    | OnAnimationFrame Time.Posix
    | MouseLeave
    | MouseDown (Point2d Pixels ScreenCoordinate)
    | MouseUp (Point2d Pixels ScreenCoordinate)
    | MouseMove (Point2d Pixels ScreenCoordinate)
    | ZoomIn
    | ZoomOut


{-| Get the camera position while taking into account any offset due to the user dragging the camera.
-}
actualCameraPosition : Model -> Point2d Pixels WorldCoordinate
actualCameraPosition model =
    case model.currentIsland of
        Just _ ->
            model.cameraWorldPosition

        Nothing ->
            case model.dragScreenStart of
                Just mouseStart ->
                    Vector2d.from
                        (screenPosToWorldPos model model.mouseScreenPosition)
                        (screenPosToWorldPos model mouseStart)
                        |> (\a -> Point2d.translateBy a model.cameraWorldPosition)
                        |> cameraInBounds model

                Nothing ->
                    model.cameraWorldPosition


cameraInBounds : Model -> Point2d Pixels WorldCoordinate -> Point2d Pixels WorldCoordinate
cameraInBounds model cameraPosition =
    let
        ( x, y ) =
            cameraPosition |> Point2d.toTuple Pixels.inPixels

        ( mapWidth, mapHeight ) =
            epiloqueMapSize |> Vector2d.toTuple Pixels.inPixels
    in
    Point2d.fromPixels
        { x = clamp 0 (mapWidth - toFloat model.windowWidth / model.cameraZoom) x
        , y = clamp 0 (mapHeight - toFloat model.windowHeight / model.cameraZoom) y
        }


port sendToJS : Json.Encode.Value -> Cmd msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UserPressedIsland island ->
            ( model, Cmd.none )

        UserPressedMenuEpilogueButton epilogue ->
            let
                readEpilogues =
                    if List.member epilogue.name model.readEpilogues then
                        model.readEpilogues

                    else
                        epilogue.name :: model.readEpilogues
            in
            ( { model
                | currentEpilogue = Just epilogue
                , currentPage = 0
                , readEpilogues = readEpilogues
              }
            , readEpilogues |> encoder |> sendToJS
            )

        UserPressedMenuCloseButton ->
            ( { model
                | currentPage = 0
                , currentIsland = Nothing
                , currentEpilogue = Nothing
                , animationStartTime = Just model.time
              }
            , Cmd.none
            )

        UserPressedMenuBackButton ->
            ( { model | currentPage = max (model.currentPage - 1) 0 }, Cmd.none )

        UserPressedMenuForwardButton ->
            ( { model
                | currentPage =
                    min
                        (model.currentPage + 1)
                        (case model.currentEpilogue of
                            Just epilogue ->
                                (epilogue.story |> List.length) - 1

                            Nothing ->
                                case model.currentIsland of
                                    Just island ->
                                        (island.defaultEpilogue |> List.length) - 1

                                    Nothing ->
                                        0
                        )
              }
            , Cmd.none
            )

        NoOp ->
            ( model, Cmd.none )

        OnWindowResize width height ->
            ( { model | windowWidth = width, windowHeight = height }, Cmd.none )

        OnAnimationFrame time ->
            ( { model | time = time }, Cmd.none )

        MouseLeave ->
            ( model
            , Cmd.none
            )

        MouseDown point ->
            let
                clickedIsland =
                    islandClickDetection model

                dragScreenStart =
                    case clickedIsland of
                        Just _ ->
                            Nothing

                        Nothing ->
                            Just point

                animationStartTime =
                    case clickedIsland of
                        Just _ ->
                            Just model.time

                        Nothing ->
                            model.animationStartTime

                zoomPoint =
                    case clickedIsland of
                        Just island ->
                            Rectangle2d.centerPoint island.rectangle
                                |> Point2d.translateBy
                                    (Vector2d.fromTuple Pixels.pixels
                                        ( toFloat model.windowWidth / -4
                                        , toFloat model.windowHeight / -2
                                        )
                                    )

                        Nothing ->
                            model.zoomPoint

                readEpilogues =
                    case clickedIsland of
                        Just island ->
                            if List.member island.name model.readEpilogues then
                                model.readEpilogues

                            else
                                island.name :: model.readEpilogues

                        Nothing ->
                            model.readEpilogues
            in
            ( case model.currentIsland of
                Just _ ->
                    model

                Nothing ->
                    { model
                        | animationStartTime = animationStartTime
                        , dragScreenStart = dragScreenStart
                        , mouseScreenPosition = point
                        , zoomPoint = zoomPoint
                        , currentIsland = clickedIsland
                        , readEpilogues = readEpilogues
                    }
            , case clickedIsland of
                Just _ ->
                    readEpilogues |> encoder |> sendToJS

                Nothing ->
                    Cmd.none
            )

        MouseUp point ->
            let
                model2 =
                    { model | mouseScreenPosition = point }
            in
            ( { model2
                | cameraWorldPosition = actualCameraPosition model2
                , dragScreenStart = Nothing
              }
            , Cmd.none
            )

        MouseMove point ->
            ( { model | mouseScreenPosition = point }
            , Cmd.none
            )

        ZoomIn ->
            ( case model.currentIsland of
                Just _ ->
                    model

                Nothing ->
                    let
                        relativeTo =
                            screenPosToWorldPos model model.mouseScreenPosition

                        newZoom =
                            min 1 (model.cameraZoom * (1 / zoomFactor))
                    in
                    { model
                        | cameraZoom = newZoom
                        , cameraWorldPosition =
                            cameraInBounds model
                                (cameraPosRelativeToZoom
                                    model.cameraWorldPosition
                                    model.cameraZoom
                                    relativeTo
                                    newZoom
                                )
                    }
            , Cmd.none
            )

        ZoomOut ->
            ( case model.currentIsland of
                Just _ ->
                    model

                Nothing ->
                    let
                        relativeTo =
                            screenPosToWorldPos model model.mouseScreenPosition

                        newZoom =
                            max (calculateMinZoom model.windowWidth model.windowHeight)
                                (model.cameraZoom * zoomFactor)
                    in
                    { model
                        | cameraZoom = newZoom
                        , cameraWorldPosition =
                            cameraInBounds model
                                (cameraPosRelativeToZoom
                                    model.cameraWorldPosition
                                    model.cameraZoom
                                    relativeTo
                                    newZoom
                                )
                    }
            , Cmd.none
            )


screenPosToWorldPos : Model -> Point2d Pixels ScreenCoordinate -> Point2d Pixels WorldCoordinate
screenPosToWorldPos model pos =
    Vector2d.from Point2d.origin pos
        |> Vector2d.scaleBy (1 / model.cameraZoom)
        |> Vector2d.toPixels
        |> Vector2d.fromPixels
        |> (\a -> Point2d.translateBy a model.cameraWorldPosition)


cameraPosRelativeToZoom : Point2d Pixels WorldCoordinate -> Float -> Point2d Pixels WorldCoordinate -> Float -> Point2d Pixels WorldCoordinate
cameraPosRelativeToZoom cameraPosition cameraZoom relativeTo newZoom =
    Vector2d.from relativeTo cameraPosition
        |> Vector2d.scaleBy (cameraZoom / newZoom)
        |> (\a -> Point2d.translateBy a relativeTo)


animationLength =
    400


calculateAnimationOffset : Model -> Float -> Float -> Bool -> Float
calculateAnimationOffset model start destination forward =
    let
        framesPassed =
            case model.animationStartTime of
                Just startTime ->
                    min animationLength (Time.posixToMillis model.time - Time.posixToMillis startTime) |> toFloat

                Nothing ->
                    animationLength
    in
    ((destination - start) / animationLength)
        * (if forward then
            framesPassed

           else
            animationLength - framesPassed
          )


calculateCameraPositionZoom : Model -> ( Point2d Pixels WorldCoordinate, Float )
calculateCameraPositionZoom model =
    let
        forward =
            case model.currentIsland of
                Just _ ->
                    True

                Nothing ->
                    False

        ( xPosition, yPosition ) =
            model.cameraWorldPosition |> Point2d.toTuple Pixels.inPixels

        ( xDestination, yDestination ) =
            model.zoomPoint |> Point2d.toTuple Pixels.inPixels

        xAnimationOffset =
            calculateAnimationOffset model xPosition xDestination forward

        yAnimationOffset =
            calculateAnimationOffset model yPosition yDestination forward

        newZoom =
            model.cameraZoom + calculateAnimationOffset model model.cameraZoom 1 forward

        ( xCameraOffset, yCameraOffset ) =
            cameraPosRelativeToZoom
                (Point2d.fromPixels
                    { x = xPosition + xAnimationOffset
                    , y = yPosition + yAnimationOffset
                    }
                )
                model.cameraZoom
                model.zoomPoint
                newZoom
                |> Point2d.toTuple Pixels.inPixels
    in
    ( actualCameraPosition
        model
        |> Point2d.translateBy
            (Vector2d.fromTuple Pixels.pixels
                ( xCameraOffset - xPosition
                , yCameraOffset - yPosition
                )
            )
    , newZoom
    )


zoomFactor =
    0.95


islandClickDetection : Model -> Maybe Island
islandClickDetection model =
    islands |> List.filter (wasIslandClicked model) |> List.head


wasIslandClicked : Model -> Island -> Bool
wasIslandClicked model island =
    let
        ( mouseX, mouseY ) =
            model.mouseScreenPosition |> Point2d.toTuple Pixels.inPixels
    in
    Rectangle2d.contains
        (Vector2d.fromPixels
            { x = mouseX / model.cameraZoom
            , y = mouseY / model.cameraZoom
            }
            |> (\a -> Point2d.translateBy a model.cameraWorldPosition)
        )
        island.rectangle


encoder : List String -> Json.Encode.Value
encoder myStrings =
    Json.Encode.list Json.Encode.string myStrings


decoder : Json.Decode.Decoder (List String)
decoder =
    Json.Decode.list Json.Decode.string


decodeData : String -> Result Json.Decode.Error (List String)
decodeData jsonString =
    Json.Decode.decodeString decoder jsonString



---- VIEW ----


view : Model -> Html Msg
view model =
    Element.layoutWith
        { options =
            [ Element.focusStyle
                { borderColor = Nothing
                , backgroundColor = Nothing
                , shadow = Nothing
                }
            ]
        }
        [ Element.height Element.fill
        , Element.width Element.fill
        , Element.clip
        , Background.color (Element.rgb 0.49 0.624 0.576)
        , Font.family [ Font.typeface "Delius", Font.sansSerif ]
        , Element.inFront mapOverlay
        , case model.currentIsland of
            Just _ ->
                Element.inFront (menu model)

            Nothing ->
                Element.inFront Element.none
        ]
        (map model)


mapOverlay : Element.Element Msg
mapOverlay =
    Element.el
        [ Element.width fill
        , Element.height fill
        , Events.onMouseLeave MouseLeave
        , Mouse.onDown (handleMouseEvent MouseDown)
            |> Element.htmlAttribute
        , Mouse.onUp (handleMouseEvent MouseUp)
            |> Element.htmlAttribute
        , Mouse.onMove (handleMouseEvent MouseMove)
            |> Element.htmlAttribute
        , Wheel.onWheel handleWheelEvent
            |> Element.htmlAttribute
        ]
        Element.none


handleMouseEvent : (Point2d Pixels ScreenCoordinate -> msg) -> { a | offsetPos : ( Float, Float ) } -> msg
handleMouseEvent msg =
    .offsetPos >> Point2d.fromTuple Pixels.pixels >> msg


handleWheelEvent : Wheel.Event -> Msg
handleWheelEvent wheelEvent =
    if wheelEvent.deltaY > 0 then
        ZoomOut

    else
        ZoomIn


map : Model -> Element.Element Msg
map model =
    let
        ( cameraOffsetPosition, cameraOffsetZoom ) =
            calculateCameraPositionZoom model

        ( offsetX, offsetY ) =
            cameraOffsetPosition
                |> Point2d.scaleAbout Point2d.origin -1
                |> Point2d.translateBy (Vector2d.scaleBy 0.5 epiloqueMapSize)
                |> Point2d.toTuple Pixels.inPixels

        ( mapWidth, mapHeight ) =
            epiloqueMapSize |> Vector2d.toTuple Pixels.inPixels
    in
    Element.el
        [ Element.moveRight offsetX
        , Element.moveDown offsetY
        ]
        (Element.image
            (List.append
                (checkmarks model)
                [ Element.width <| Element.px (round mapWidth)
                , Element.height <| Element.px (round mapHeight)
                ]
            )
            { src = "EpilogueMap.jpg", description = "THE MAP" }
        )
        |> Element.el [ Element.scale <| cameraOffsetZoom ]
        |> Element.el
            [ Element.moveLeft <| (mapWidth / 2)
            , Element.moveUp <| (mapHeight / 2)
            ]


epiloqueMapSize =
    Vector2d.pixels 6000 4000


checkmarks : Model -> List (Element.Attribute msg)
checkmarks model =
    islands |> List.map (generateCheckmark model)


generateCheckmark : Model -> Island -> Element.Attribute msg
generateCheckmark model island =
    let
        ( x, y ) =
            List.head (Rectangle2d.vertices island.rectangle)
                |> Maybe.withDefault (Point2d.fromTuple Pixels.pixels ( 0, 0 ))
                |> Point2d.toTuple Pixels.inPixels

        islandStoryNames =
            List.concat [ List.map (\a -> a.name) island.epilogues, [ island.name ] ]
    in
    Element.inFront
        (if
            Set.fromList islandStoryNames
                |> Set.intersect (Set.fromList model.readEpilogues)
                |> (==) (Set.fromList islandStoryNames)
         then
            Element.image
                [ Element.moveRight (x - 20)
                , Element.moveDown (y - 30)
                ]
                { src = "checkmark.png", description = "Checkmark" }

         else
            Element.none
        )


menu : Model -> Element.Element Msg
menu model =
    Element.image
        [ Element.inFront (menuItems model)
        , Element.inFront closeMenuButton
        , Element.scale (calculateScaleFactor model)
        , Element.alignRight
        , Element.alignTop
        , Element.moveRight ((calculateScaleFactor model - 1) * -480)
        , Element.moveUp ((calculateScaleFactor model - 1) * -540)
        ]
        { src = "Menu.png", description = "THE MENU" }


closeMenuButton : Element.Element Msg
closeMenuButton =
    Input.button
        [ Element.moveRight 40, Element.moveDown 35 ]
        { onPress = Just UserPressedMenuCloseButton
        , label =
            Element.image
                []
                { src = "X.png", description = "close menu" }
        }


calculateScaleFactor : Model -> Float
calculateScaleFactor model =
    min (toFloat model.windowWidth / 1920) (toFloat model.windowHeight / 1080)


menuItems : Model -> Element.Element Msg
menuItems model =
    let
        epilogueButtons =
            case model.currentIsland of
                Just island ->
                    menuButtons island.epilogues model

                Nothing ->
                    []

        paragraphText =
            case model.currentEpilogue of
                Just epilogue ->
                    List.head (List.drop model.currentPage epilogue.story)
                        |> Maybe.withDefault ""

                Nothing ->
                    case model.currentIsland of
                        Just island ->
                            List.head (List.drop model.currentPage island.defaultEpilogue)
                                |> Maybe.withDefault ""

                        Nothing ->
                            ""

        titleText =
            case model.currentEpilogue of
                Just epilogue ->
                    epilogue.name

                Nothing ->
                    case model.currentIsland of
                        Just island ->
                            island.name

                        Nothing ->
                            ""
    in
    Element.column
        [ Element.inFront (pageCount model)
        , Element.inFront (backButton model)
        , Element.inFront (forwardButton model)
        , Element.inFront (authorText model)
        , Element.width fill
        , Element.height fill
        ]
        [ Element.row
            [ Element.width fill
            , Element.height (Element.px 230)
            , Element.paddingEach { top = 15, right = 50, bottom = 80, left = 150 }
            ]
            epilogueButtons
        , Element.el
            [ Font.size 40
            , Font.center
            , Element.paddingXY 100 10
            , Font.family [ Font.typeface "Aladin" ]
            , Font.color (Element.rgb255 55 30 20)
            , Element.width fill
            ]
            (Element.text titleText)
        , Element.el
            [ Element.width fill
            , Element.paddingXY 100 16
            , Font.size 20
            ]
            (Element.html
                (Html.div [ Attributes.class "epilogue-text" ] <|
                    Html.Parser.Util.toVirtualDom
                        (case Html.Parser.run paragraphText of
                            Result.Ok nodeList ->
                                nodeList

                            _ ->
                                []
                        )
                )
            )
        ]


backButton : Model -> Element Msg
backButton model =
    if model.currentPage == 0 then
        Element.none

    else
        Input.button [ Element.moveRight 340, Element.moveDown 950 ]
            { onPress = Just UserPressedMenuBackButton
            , label =
                Element.image
                    []
                    { src = "LeftArrow.png", description = "go back" }
            }


forwardButton : Model -> Element Msg
forwardButton model =
    if
        model.currentPage
            == (case model.currentEpilogue of
                    Just epilogue ->
                        List.length epilogue.story - 1

                    Nothing ->
                        case model.currentIsland of
                            Just island ->
                                List.length island.defaultEpilogue - 1

                            Nothing ->
                                0
               )
    then
        Element.none

    else
        Input.button [ Element.moveRight 600, Element.moveDown 950 ]
            { onPress = Just UserPressedMenuForwardButton
            , label =
                Element.image
                    []
                    { src = "RightArrow.png", description = "go back" }
            }


pageCount : Model -> Element Msg
pageCount model =
    let
        totalPage =
            case model.currentEpilogue of
                Just epilogue ->
                    List.length epilogue.story

                Nothing ->
                    case model.currentIsland of
                        Just island ->
                            List.length island.defaultEpilogue

                        Nothing ->
                            0
    in
    if totalPage > 1 then
        Element.el
            [ Element.moveRight 25
            , Element.moveDown 960
            , Font.size 30
            , Font.family [ Font.typeface "Aladin" ]
            , Font.color (Element.rgb255 110 85 65)
            , Font.center
            , Element.width fill
            ]
            (Element.text
                ("Page "
                    ++ String.fromInt (model.currentPage + 1)
                    ++ " of "
                    ++ String.fromInt totalPage
                )
            )

    else
        Element.none


authorText : Model -> Element Msg
authorText model =
    let
        output author =
            Element.el
                [ Element.moveDown 285
                , Font.size 18
                , Font.family [ Font.typeface "Aladin" ]
                , Font.color (Element.rgb255 110 85 65)
                , Font.center
                , Element.width fill
                ]
                (Element.text
                    ("By: "
                        ++ author
                    )
                )
    in
    case model.currentEpilogue of
        Just epilogue ->
            if String.length epilogue.author > 0 then
                output epilogue.author

            else
                Element.none

        Nothing ->
            case model.currentIsland of
                Just island ->
                    if String.length island.defaultAuthor > 0 then
                        output island.defaultAuthor

                    else
                        Element.none

                Nothing ->
                    Element.none


menuButtons : List Epilogue -> Model -> List (Element Msg)
menuButtons epiloguesList model =
    epiloguesList |> List.map (generateEpilogueButton model)


generateEpilogueButton : Model -> Epilogue -> Element Msg
generateEpilogueButton model epilogue =
    Element.el [ Element.width fill, Element.height fill ]
        (Input.button
            [ Element.centerX
            , Element.centerY
            , Element.inFront
                (if epilogueUnlocked model epilogue then
                    Element.none

                 else
                    Element.image
                        [ Element.centerX
                        , Element.centerY
                        ]
                        { src = "Lock.png", description = "epilogue is locked" }
                )
            ]
            { onPress =
                if epilogueUnlocked model epilogue then
                    Just (UserPressedMenuEpilogueButton epilogue)

                else
                    Just (UserPressedMenuEpilogueButton (lockedEpilogue epilogue.prerequisites))
            , label =
                Element.image
                    [ Border.rounded 25
                    , Border.width 10
                    , Border.color
                        (if List.member epilogue.name model.readEpilogues then
                            Element.rgb 0.2 0.6 0.1

                         else
                            Element.rgb255 34 11 0
                        )
                    , Element.scale 0.9
                    ]
                    { src = epilogue.image, description = epilogue.name }
            }
        )


type alias Island =
    { name : String
    , rectangle : Rectangle2d Pixels WorldCoordinate
    , epilogues : List Epilogue
    , defaultAuthor : String
    , defaultEpilogue : List String
    }


lockedEpilogue : List String -> Epilogue
lockedEpilogue prerequisites =
    { name = "Locked"
    , image = ""
    , prerequisites = []
    , author = ""
    , story = [ "This epilogue is locked behind " ++ (List.map (\a -> "\"" ++ a ++ "\"") prerequisites |> List.intersperse " and " |> String.concat) ++ "." ]
    }


epilogueUnlocked : Model -> Epilogue -> Bool
epilogueUnlocked model epilogue =
    Set.fromList epilogue.prerequisites
        |> Set.intersect (Set.fromList model.readEpilogues)
        |> (==) (Set.fromList epilogue.prerequisites)



---- SUBSCRIPTION ----


subscriptions model =
    Sub.batch
        [ Browser.Events.onResize OnWindowResize
        , Browser.Events.onAnimationFrame OnAnimationFrame
        ]



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }



---- ISLANDS ----


islands : List Island
islands =
    [ { name = "Gardelon"
      , rectangle = Rectangle2d.from (Point2d.pixels 654 2767) (Point2d.pixels 998 3061)
      , epilogues = Epilogues.gardelon
      , defaultAuthor = "SA"
      , defaultEpilogue = [ "<p>&emsp;&emsp;After the defeat of Omen Diamondbody and the dissolving of the United World Army, Gardelon saw a period of mass political reform. It wasn’t easy transitioning from a monarchy to a socialist democracy, but morale was high from the recent victory against the marines and the liberation of Gardelon before that. Cecilia continued to serve as a figurehead and moral compass, while William was democratically elected to a seat of power in the new parliament. They both benefited from much more manageable workloads, and all but the most rigid traditionalists eventually agreed that this new system made a lot more sense. Over time, the class disparity between the High and the Low narrowed considerably, Gardelon relaxed its control over the rest of the Violet Ocean, and the impossibly long list of laws was shortened down to a mere thirty thousand or so. Gardelon saw an era of prosperity like never before and Cecilia’s dream… was fulfilled.</p>" ]
      }
    , { name = "Brittlepoint Mohole"
      , rectangle = Rectangle2d.from (Point2d.pixels 76 3207) (Point2d.pixels 354 3400)
      , epilogues = Epilogues.brittlepoint
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;With the help of Gardelon and the new World Marines, Brittlepoint Mohole became a safer place to be. Tourists could stay out until as late as 12:30 AM without fear of monsters overrunning the island and murdering them, an entire thirty minutes of precious vacation time that never existed before. The rewards for adventuring there were increased, and daredevils from all over the world came to test their mettle there. Many precious artifacts were found in the depth of the pit, as well as monsters never before seen. Countless advancements in the field of biology are owed directly to what later became known as the Second Brittlepoint Rush.  </p>"
            ]
      }
    , { name = "Shifting Island"
      , rectangle = Rectangle2d.from (Point2d.pixels 3206 3444) (Point2d.pixels 3426 3607)
      , epilogues = Epilogues.shifting
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The Shifting Island eventually became home to a small village called Neversteady, whose inhabitants were perfectly okay with the, well, never steady nature of the place. In mornings it was humid, at nights too cold. By day it was a grasslands, by night it was a forest. By day it would be storming, by night it would be starry and wonderful. It was a haven for those who wanted to live peacefully, but never too peacefully. Unfortunately, this meant occasionally being battered by horrific storms and needing marine assistance, but eh. That was life.</p>"
            ]
      }
    , { name = "Gnehah Archipelago"
      , rectangle = Rectangle2d.from (Point2d.pixels 4476 3522) (Point2d.pixels 4685 3717)
      , epilogues = Epilogues.gnehah
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The archipelagos were abandoned when the World Tree took off. As important and interesting as their coral research was, none of the elves there valued it over returning home. The few people living in the area cleared out with them, and no one thought of Gnehah for a long while. A couple decades later the underwater laboratories were found by pirates and, horrified by what they had stumbled onto, they torched everything in the labs and refused to ever speak about the secrets of the archipelagos' coral.</p><p>&emsp;&emsp;Gnehah was left empty after that, the laboratories barely serving as even a reminder that something was once there.</p>"
            ]
      }
    , { name = "The Chasm Key"
      , rectangle = Rectangle2d.from (Point2d.pixels 3881 3581) (Point2d.pixels 4129 3822)
      , epilogues = Epilogues.chasm
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;For a few years after the great war Jiopasd saw a decline. The call for burly adventurers had left much of the world and everyone wanted to bask in peace. There were nights the Steel Serpent was entirely empty save for the one-eyed barman who manned it. For two former marines – one old and one young – who had settled at the Chasm Key, this suited them just fine. There were just enough tough guys around to repel the occasional monster that slithered up from the chasms, but never enough to start a ruckus.  </p><p>&emsp;&emsp;Incidents in Brittlepoint Mohole eventually lead to a new call for adventurers, however, and the Chasm Key blossomed once more. People started calling Jiopasd 'The Beginner's Brittlepoint', and it became the first stop for many adventurers looking to become stronger. </p>"
            ]
      }
    , { name = "Prehistoric Island"
      , rectangle = Rectangle2d.from (Point2d.pixels 3526 2987) (Point2d.pixels 3837 3186)
      , epilogues = Epilogues.prehistoric
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The prehistoric island became a gathering spot for scientists, particularly biologists and mineralogists, although a particular group of astrologists also met there infrequently. One of these groups stumbled onto a well-hidden camp belonging to a tribe of natives thought wiped out long long ago. Luckily they were friendly, and in exchange for modern goods were willing to pass on their knowledge of how to live side by side with the prehistoric beasts of the island.  </p><p>&emsp;&emsp;Over the years this led to the island becoming a giant petting zoo. Entrepreneurs set up trade deals with the locals and enlisted their help to domesticate the entire island until it was safe enough for people all over the Green to come visit.</p>"
            ]
      }
    , { name = "Poblan"
      , rectangle = Rectangle2d.from (Point2d.pixels 4890 2179) (Point2d.pixels 5050 2342)
      , epilogues = Epilogues.poblan
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Poblan settled into peace after the great war and set up lucrative trade deals with islands the world over, becoming known as the premier place for quality fish. Unojo Akbolto continued to act as the village's protector and face. He eventually reunited with his brother when Ventus and Marie stopped by for a visit, and the two of them bonded over their separate and meaningful collisions with the Fellowship pirates. </p><p>&emsp;&emsp;About two decades later, Poblan's population vanished. It became one of the world's great mysteries. There was blood and signs of a major struggle, but it was like the bodies had all gotten up and walked away afterwards. People even reported seeing some of their missing loved ones from Poblan years later in various places across the oceans.  </p><p>&emsp;&emsp;Reports were launched, and investigations opened, but ultimately a culprit was not discovered. </p>"
            ]
      }
    , { name = "Heahea Station"
      , rectangle = Rectangle2d.from (Point2d.pixels 3864 3200) (Point2d.pixels 4083 3400)
      , epilogues = Epilogues.heahea
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p><strong>ATTENTION </strong></p><p><br></p><p>Want a haircut no one's ever seen before? Want the dress that all the celebrities are wearing? Want dye in a color that didn't actually exist prior to your asking?  </p><p><br> </p><p>Well, what other island could you go to? </p><p><br></p><p>Under Mayor Otto von Wobbler, business has been booming booming booming! Make SURE you book your appointments well in advance! Internships are always available for those up and coming aesthicians, and volunteers are welcome! </p>"
            ]
      }
    , { name = "Newheart"
      , rectangle = Rectangle2d.from (Point2d.pixels 4222 3114) (Point2d.pixels 4646 3411)
      , epilogues = Epilogues.newheart
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;With Diamondbody's demise and the World Marines' shift to a more peaceful organization, the demand for oceanrock dropped off, and Newheart's pirate problems dropped off with it. The island went back to mining for other, more industrious ores and kept up a good trading profile through them. Eventually all the damage from Agua's typhoon was repaired, but the loss of the volcano was never truly recovered from.</p><p>&emsp;&emsp;A modest stone statue of the Fellowship Pirates was constructed just inside the city limits one year on the anniversary of Newheart's rescue. It remained impeccably maintained over the years by those dwarves who had been present for the moment.</p>"
            ]
      }
    , { name = "The Worldtree"
      , rectangle = Rectangle2d.from (Point2d.pixels 812 2029) (Point2d.pixels 1129 2250)
      , epilogues = Epilogues.worldtree
      , defaultAuthor = "ML"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;It was quite a shock to the Elven fleets when the Worldtree returned. Had it not disappeared without a trace centuries before? Had they not already mourned its loss and moved on, as all elves must? This was an impossibility, and yet the Worldtree was here. Naturally, the fleets were overjoyed… in their own way.</p><p>&emsp;&emsp;Where the Worldtree was once empty, it was now bustling. Curious elves flooded it's halls, constantly questioning the Worldtree's crew about their marooning on a mysterious frontier world. The Arc Amplifier was shown to an amazed crowd, and the wider fleet's advancements were shared in turn. For the first time in centuries, the Lord-Admiral of the Worldtree approved the development of a new generation. None of them resembled Tanis Silverhand.</p>"
            ]
      }
    , { name = "Halapanos"
      , rectangle = Rectangle2d.from (Point2d.pixels 4870 1874) (Point2d.pixels 5056 2037)
      , epilogues = Epilogues.halapanos
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Sporting the largest volcano in the Green Ocean, Halapanos became a tourist destination for dwarves in that corner of the world after the cradle was reignited. One particular dwarf quit a life of famous pirating to settle back down in Halapanos and become a tour guide there. His unruly crew came with him, and the whole lot of them created something akin to a tour-guide mafia business, forcing tourists to come view the sights of Halapanos or else risk being roughed up. </p><p> &emsp;&emsp;Unfortunately for them, the new World Marines caught on very fast, having a base in the area and a vested interest in keeping islands with Storm Travellers safe, and they were kicked out quickly.  </p>"
            ]
      }
    , { name = "Prosperity"
      , rectangle = Rectangle2d.from (Point2d.pixels 5131 1432) (Point2d.pixels 5382 1645)
      , epilogues = Epilogues.prosperity
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;When the Alchemy Crystum was absorbed into the Cradle, much of Prosperity vanished. The island's foundation was mercifully built with pre-existing materials, but many of the more lavish buildings, the city's streets, public amenities, and more were built with the crystum's power.  </p><p>&emsp;&emsp;The city's true nature shined through in the hard times after the war, though, and a community that most of the Red Ocean regarded as spoiled and lazy came together to rebuild their city back to its sparkling self, funded off the back of wealthy investors Miss Mary Midas had helped in the past. A few years later, it was hard to tell Prosperity had ever fallen apart.  </p>"
            ]
      }
    , { name = "Heaven's Crown"
      , rectangle = Rectangle2d.from (Point2d.pixels 2669 1496) (Point2d.pixels 2904 1921)
      , epilogues = Epilogues.heavenscrown
      , defaultAuthor = "MM"
      , defaultEpilogue =
            [ "<p> &emsp;&emsp;For humans, all things must come to an end - and though Adra had the soul of a dragon, she was a human too. She and Alexis lived together, loved together, lost together, grew old together… And, eventually, died. A part of Adra passed away, her life pouring out into the world that nurtured her alongside the families she had lost and the woman she loved.</p><p> &emsp;&emsp;And something continued on.</p><p> &emsp;&emsp;That night, the Red Ocean was painted scarlet once again as a spirit of fire came to Heaven’s Crown to forge its body. Polaris rose, and though it was not entirely what had come before, it was not entirely different either - for its was to understand where it came from, and how that would lead to the future.</p><p> &emsp;&emsp;Polaris protected the world and watched over the descendants of Adra’s family for eons as the Dragon of the North Star, until one day, like Draconis before it, its time came to pass from its place in this world’s sky, and ascend to fly the cosmos.</p><p> &emsp;&emsp;In a certain family, however, a beautiful glass flute continued to be passed down, along with this instruction: Should you find yourself in trouble, my child, play this flute; the light of Polaris will return to protect you.</p>"
            ]
      }
    , { name = "The Citadel"
      , rectangle = Rectangle2d.from (Point2d.pixels 3440 761) (Point2d.pixels 3694 1389)
      , epilogues = Epilogues.citadel
      , defaultAuthor = "BB"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The monolithic Citadel, standing tall and timeless, was slowly but surely repurposed by those it had sheltered for millennia. Within its countless floors, the best and brightest of terran science and industry set in motion the reinvention of their society’s technology from the ground up. As weeks passed into months, their ceaseless labor churned out wonder after wonder. Invitations were sent across the world, to all discerning scholars and engineers. Come to the Citadel, and if you put in the work, you shall see the impossible made reality. Their secrets and wonders would be shared with all, and decades of technological advancement were accomplished within the span of weeks. The Vavylon, reborn, opened its doors again.</p><p>&emsp;&emsp;Eureka Sparkrow’s laboratories were demolished. To the terrans, they were a horror show and a stark example of science gone too far. Only his museum was spared destruction, which was renovated and filled with examples of his experiments. It would come to serve as a parable on the pursuit of knowledge without conscience; a bleak charnel house forever lurking in the shadow of progress.</p>"
            ]
      }
    , { name = "The World Information Center"
      , rectangle = Rectangle2d.from (Point2d.pixels 3521 1435) (Point2d.pixels 3636 1645)
      , epilogues = Epilogues.wic
      , defaultAuthor = "BB"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;When Edward and his crew of revolutionaries came to the World Information Center, they found an island in flames. They rushed into port, beating back the inferno to the best of their abilities. There was nobody here, and no signs of a struggle. The only answer could be self-sabotage.</p><p>&emsp;&emsp;As his crew swept the island, doing their best to mitigate further damages, Edward found the sole remaining inhabitant, sitting in his office and enjoying a cup of tea as fire devoured his life’s work all around him. Commander L'écriture.</p><p>&emsp;&emsp;So it was that Edward “Eddy” Ettekur of the Fellowship Knights and the mastermind of Omen Diamondbody’s propaganda machine sat across from each other and debated ideology. It was a verbal duel that both had long awaited, but the roaring flames made it clear from the start who the victor truly was.</p><p>&emsp;&emsp;L'écriture left the WIC, hanging from a Bird of Flight. He would rather the center burn than capitulate it to his enemy. If Eddy wanted to get his account of the Fellowship’s story out to the world, he would need to deliver it himself…</p><p>&emsp;&emsp;And as crews of terrans flocked to the call to rebuild and repurpose the WIC, that is just what Edward and his companions did.</p>"
            , "<p>&emsp;&emsp;It took years. Longer, certainly, than it had taken for the actual tale of the Fellowship to begin and end. Every place Eddy visited, he offered two things: a comprehensive account of the struggle against Omen’s regime, and the construction of a relay tower. These strange towers were able to transmit and receive signals from larger structures built by the edges of the storm belts, who in turn communicated with the central network hub which had now come to dominate the island on which the WIC once stood.</p><p>&emsp;&emsp;In this global relay system, a massive digital network was realized. Edward and his revolutionary cohort had created the internet. It was primitive, it was patchy, and it was unwieldy, but it worked. With it, the dissemination of information and ideas was put in the hands of the masses, not the powers that be. The world was closer and more interconnected than ever before, and systems of power and authority found themselves held to scrutiny from far and wide. It was a revolution. A people’s revolution.</p>"
            ]
      }
    , { name = "The Frozen Fog"
      , rectangle = Rectangle2d.from (Point2d.pixels 4446 0) (Point2d.pixels 5069 314)
      , epilogues = Epilogues.frozenfog
      , defaultAuthor = "SA"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Without the life crystum to keep it alive, the Great Tree’s age caught up with it and over time it rotted away and collapsed under its own weight. The island around it began to fracture and erode without the ancient roots to hold it together, making life extremely difficult. Fortunately, the orcs managed to leave and find a new home in The Rings before the tree collapsed entirely. The oldest and mightiest tree in the world was reduced to nothing but driftwood, but that’s simply the nature of life: Nothing lasts forever.</p>"
            ]
      }
    , { name = "The Rings"
      , rectangle = Rectangle2d.from (Point2d.pixels 4527 2181) (Point2d.pixels 4707 2323)
      , epilogues = Epilogues.rings
      , defaultAuthor = "SA"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;After being largely unpopulated for so long, the Rings suddenly found themselves being colonized by a large group of orc refugees. What most people considered a harsh environment unsuitable for much more than hiding out, the orcs found to be the perfect new home. With the guidance of their new shaman they built villages along the rocky shores and tamed the local wildlife. It took a lot of effort before they began thriving again, but if there’s one thing to be said for orcs it’s that they’re tenacious.</p><p>&emsp;&emsp;It was difficult at first for them to grow accustomed to living without a protective wall of fog surrounding them, but with time they learned to not fear the outside world. With time, orcs became a common sight throughout the green ocean, trading hand-crafted goods and offering their services as warriors and animal-trainers. Though they lamented their drastically shortened lives, the orcs all agreed that a year spent surrounded by the endless blue horizons with a blanket of stars above was worth more than a whole decade of nothing but gray in all directions.</p>"
            ]
      }
    , { name = "Arc Island"
      , rectangle = Rectangle2d.from (Point2d.pixels 4230 3660) (Point2d.pixels 4422 3866)
      , epilogues = Epilogues.arc
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Cor Leonis settled down on Arc Island, guiding it into a sort of minor prosperity as the eras progressed. The once laughably dragon cult grew and grew, until one day it was no long recognizable for what it once was. The Scaled Knights of Arc-Cor were feared all through the Green Ocean, though they never left their titular island. Tourism to Arc Island stopped, to Cor Leonis's pleasure, and this small piece of his territory remained peaceful for all his eternal life. </p><p>&emsp;&emsp;For Simon Sezz and the other inhabitants of Crescent City, Cor Leonis's choice to settle in was a fantastic one. The dragon rewarded his devoted worshippers with endless wealth and their city prospered.</p>"
            ]
      }
    , { name = "Violaverde"
      , rectangle = Rectangle2d.from (Point2d.pixels 2615 2416) (Point2d.pixels 2822 2563)
      , epilogues = Epilogues.violaverde
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The words \"economic collapse\" are thrown around a lot these days, but the great market implosion of Violaverde truly exemplified them. The purple half of the island started to undergo vicious inflation as decreasing prices on the green half of the island allowed shoppers to the island to have more money to spend there. Then, falling into a conspicuous consumption fallacy, shoppers began to see the high prices of the purple side of the island as a sign of quality and started to shop there exclusively, driving the inflation higher and higher. Overnight, the merchants on the green side got together and decided to skyrocket their prices past the purple side's to compete. Eventually no one could actually afford to shop at Violaverde anymore, but nor did they <em>want</em> to shop there if the prices were lowered.</p><p>&emsp;&emsp;Ironically, this inflation issue was eventually solved by a particular former Disaster who sailed in with large bags of money, bought the entire island, and set extremely fair price ceilings on everything.</p>"
            ]
      }
    , { name = "Fire Dragon Island"
      , rectangle = Rectangle2d.from (Point2d.pixels 2403 3052) (Point2d.pixels 2554 3221)
      , epilogues = Epilogues.firedragon
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;One little island in the southern storm belt was never again found.</p><p>&emsp;&emsp;Even as the Storm Belts shrank and eventually faded altogether, the little island remained anonymous between the Violet and Green Oceans.  </p><p>&emsp;&emsp;It wasn't a lonely isolation, however, but rather a peaceful one. The island faded from the maps over time and was granted the reward of silence for its service in saving the world.</p>"
            ]
      }
    , { name = "The Enclave of Sakurai"
      , rectangle = Rectangle2d.from (Point2d.pixels 1800 3055) (Point2d.pixels 1920 3190)
      , epilogues = Epilogues.sakurai
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Nothing on the Enclave of Sakurai changed at all, and in fact it wasn't impacted by the great war or the resurrection of the Three at all. It was remarkably stable. </p><p>&emsp;&emsp;Until the eighth Emblem of Fire arrived. </p><p>&emsp;&emsp;Her name was Lethby, and she wielded a sword of unholy flame. She went from platform to platform, defeating every single warrior in the enclave, eventually coming to the great crystal at the heart of the island that had protected it for generations. There she faced off against the other seven Emblems of Fire and defeated them all. During the fight the great crystal shattered, and the veil of bad luck that had protected the enclave for generations was dispersed forever. </p><p>&emsp;&emsp;Of course, none of this mattered to anyone outside of the enclave. People generally just continued to pretend it didn't exist. </p>"
            ]
      }
    , { name = "Port Flannigan"
      , rectangle = Rectangle2d.from (Point2d.pixels 1860 3795) (Point2d.pixels 2133 3927)
      , epilogues = Epilogues.flannigan
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p> A flier: </p><p> <br></p><p>\"FEELING DOWN? WE'VE GOT JUST THE THING! </p><p> <br></p><p>Come to Port Flannigan! Bring your family! </p><p> <br></p><p>Our therapeutic services are second to none! </p><p> <br></p><p>Please, bring your entire family! Bring friends, too!  </p><p> <br></p><p>We GUARANTEE that no one leaves our island unhappy! In fact, once you're here you won't want to leave at all! </p><p> <br></p><p>Ever! </p><p> <br></p><p>Bring everyone you know!\" </p><p> <br></p><p>Marker at the bottom reads, in small faltering handwriting: it's hungrier </p>"
            ]
      }
    , { name = "Ashwaki’s Haven"
      , rectangle = Rectangle2d.from (Point2d.pixels 1068 3333) (Point2d.pixels 1352 3512)
      , epilogues = Epilogues.ashwaki
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The negotiations around Ashwaki's Haven became the talk of legends amongst lawyers of the world. The Haveners, as they called themselves, naturally had a claim to the island. The elves, eager to finish repairs on the World Tree, felt they had the right to reclaim the engine that served as the base of the island. Helping the elves' argument was the fact that Ashwaki's Haven was a well-known, well, haven for raiders. For decades Gardelonian merchant ships had been plagued by attacks from bandits who called the World Tree's fallen engine home. Nonetheless it was the lawyers of Gardelon who were sent to argue in favor of the Haveners after William and Cecilia decided that letting the elves reclaim the engine might set a bad precedent for other islands across the oceans that were built on the back of elven technology (or any other borrowed ancient items, for that matter). </p><p>&emsp;&emsp;The resultant legal battle became known as the Grey Haired Lectures, both because of how long the debates raged for, but also because the stress of them turned the Gardelonian lawyers' hair grey (the elvish lawyers came in with grey hair). The end result was the elves reclaiming their engine, but in return building a similar structure in its place, which contented everyone well enough, except the merchant ships which transported the resources to build said structure, all of which were plundered en-route on their first voyage. </p>"
            ]
      }
    , { name = "Caratz Island"
      , rectangle = Rectangle2d.from (Point2d.pixels 550 3543) (Point2d.pixels 750 3723)
      , epilogues = Epilogues.caratz
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;\"You want my treasure? You can have it! I left everything I own in One Chunk, at the end of the Great Circle! Now you just have to find it!\" </p><p>&emsp;&emsp;Such were the last words of the final heir of the Caratz family before he died peacefully in his sleep, sparking a new era of treasure hunting across the world. Pirates and marines the world over searched for the One Chunk, said to possess enough treasure to make its finder the Treasure Hunting King.  </p><p>&emsp;&emsp;One particular boy, a resident of Caratz Island, Spelunky Z. Goofy, always dreamed of being the Treasure Hunting King. He went on to form a fellowship full of wacky and assorted people of various races and powers and dreams, and they- well, this is a story for another time, isn't it? It'd take hundreds, nearly a thousand or more, chapters to tell. </p>"
            ]
      }
    , { name = "The Enris"
      , rectangle = Rectangle2d.from (Point2d.pixels 407 2490) (Point2d.pixels 472 2555)
      , epilogues = Epilogues.enris
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>For the millionth day in a row, the wind blows calmly across a tiny patch of land in the Violet Ocean.  </p><p> <br></p><p>The soft grass on it rustles just a bit in response. Small waves lap at its shore. </p><p> <br></p><p>All is peaceful and undisturbed. All is forgotten. </p>"
            ]
      }
    , { name = "Synesthe, the Island of Song"
      , rectangle = Rectangle2d.from (Point2d.pixels 559 2022) (Point2d.pixels 750 2258)
      , epilogues = Epilogues.synesthe
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;Synesthe was quiet in the months leading up to the great war. Music still floated out of it, certainly, but the Gale Crystum rarely, if ever, rang out its beautiful tones through the spires. The Conductor spent all his time inside, frustrated by the ever-present marines that had all but taken over the island, but in his confinement he was working tirelessly on his greatest composition. When the World Marines fell and the marines stationed in Synesthe cleared out, a concert more beautiful than anyone in the Violet Ocean had ever heard before rang out through the spires, repeating itself every day for a week.</p>"
            ]
      }
    , { name = "Eisensyde"
      , rectangle = Rectangle2d.from (Point2d.pixels 1285 2026) (Point2d.pixels 1480 2236)
      , epilogues = Epilogues.eisensyde
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;After the war, Eisensyde set up a trade deal with Gardelon, providing the kingdom a steady supply of oceanrock from the backs of the Miracle Shell turtles, which prospered peacefully under the part of the deal that required the research institute be left alone except for the biannual oceanrock deliveries. </p><p>&emsp;&emsp;In the wake of the Worldtree's departure, Shelly Myter was gone. Her robotic sentries continued to carry out her deliveries for a time, but they were far from immune to the ravages of time, and after years without repairs, they began to break down. The turtles, too, began to empty out of Eisensyde after decades without their caretaker. They began a slow migration into the Heart of the Storm, which welcomed them readily.  </p>"
            ]
      }
    , { name = "The Gate of Gardelon"
      , rectangle = Rectangle2d.from (Point2d.pixels 2142 2448) (Point2d.pixels 2384 2679)
      , epilogues = Epilogues.gate
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;The citizens of Linwane evicted the last of the marines shortly after the end of the great war and took control of the Storm Surger for themselves. Now with the ability to collect fees on the passage, the town saw an economic boom, which was then met with a corresponding increase in crime as the smugglers who had long called the Violaverde-Gate route home came out of the woodwork en-masse to offer cheaper transport. After a couple years, this led Linwane's city council to invite back the newly reformed World Marines to help police the route. </p><p>&emsp;&emsp;This led to a multi-year cycle in which the marines would be invited back, only for the city to see profits fall, at which point they would be asked to leave, at which point smugglers would undercut the city, at which point the marines would be invited back... so on and so on, until the day storm travelers stopped being so necessary and funds began to dry up for all parties. </p><p>&emsp;&emsp;It was around this time that the Gate of Gardelon itself, the ancient stone structure that had watched over the Violet Ocean for as long as anyone save the Fourth could remember, began to break apart. Millenia of erosion finally began to overtake the gate, but before disaster could come to the monument, the smugglers, marines, and citizens of Linwane found a way to set aside their differences and pool their resources to hire earthspeakers from the Archipelagos to save the Gate of Gardelon.   </p><p>&emsp;&emsp;From that day forward, the marines and smugglers worked together for the good of the city, rather than against each other.</p>"
            ]
      }
    , { name = "Cherona"
      , rectangle = Rectangle2d.from (Point2d.pixels 960 1334) (Point2d.pixels 1277 1563)
      , epilogues = Epilogues.cherona
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;When the World Marines came under new leadership, much talk was had about what to do with the \"island\" of Cherona. For centuries, the turtle had housed life, from Ascheron to the marines, but at the end of the day it was a living thing, and as important as the marine base on its back was, many marines began to feel that they were exploiting the turtle.  </p><p>&emsp;&emsp;The turtle seemed to understand instinctively the debate surrounding it, and one day made the choice of what to do very easy: it diverted from its natural paths and began to swim into the Storm, its slow trajectory giving the marines plenty of time to abandon their base. On the day Cherona finally breached the storm, dozens of marines once stationed on it gathered on distant ships to watch with spyglasses as unworldly rains and winds smashed against ancient buildings, tumbling them into the ocean and freeing the great turtle at last. Not one of the marines bore any resentment. </p>"
            ]
      }
    , { name = "The Dwarven Archipelagos"
      , rectangle = Rectangle2d.from (Point2d.pixels 75 1333) (Point2d.pixels 540 2262)
      , epilogues = Epilogues.archipelagos
      , defaultAuthor = "RK"
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;In the years following the fall of Omen Diamondbody, the dwarves flourished. The Cradle having been restored by Wolsh and the Lazurusite, fully repowered the Archipelago and the surrounding islands that had once been part of the god of earth. Gaillé was but the first of many new dwarves to awake in the time that followed, and soon the dwarven lands were once again full of people. </p><p>&emsp;&emsp;The loss of Wolsh and Elder Corrai and the many warriors who fell in the final battle against the Marines was deeply felt among dwarven kind. The caldera, still wary but more welcoming than it ever had been, allowed some dwarves to return to its shores. It was decided by the Council and Tribune that all those who had fallen that day would be given traditional burial, with all honors, along the peak of the caldera. Renamed Heroes’ Rest, it was now home to the tombs of not only Durin, but the many heroes of the modern era. </p><p>&emsp;&emsp;Caledon, once disregarded as an outer territory of the Archipelago, gained new status among the people of Durin. Athair and his ward made a home there, one that was frequently visited by all manner of guests, not the least of which were members of the now legendary Fellowship. </p><p>&emsp;&emsp;Eagel stepped down as Tribune after the war, and was replaced by a series of leaders who attempted to move the Archipelago into the future (albeit slowly). The dwarves were nevertheless instrumental in the reformation of the World Marines into a proper peacekeeping force. The dwarves were staunch allies of its new leadership and provided a great deal of manpower and materiel in those early days of change. </p>"
            , "<p>&emsp;&emsp;On the whole, dwarves began to look further afield, remembering that there was a world beyond their borders, one which not only might be of help to them, but which they would be able to aid as well. Dwarven-Marine ships began to patrol widely in the White Ocean and the Violet, protecting islands such as Twinlights, Silverstone, and more. New bonds were forged with their neighbors and old promises were renewed, leading to closer relationships with Gardelon, Aashita, and even the Worldtree. The dwarves also welcomed (after a bit of adjustment) many of the awakened machinelings into their ranks.</p><p>&emsp;&emsp;The dwarves had regained their hope, their fire, their will to live as part of the world.</p>"
            ]
      }
    , { name = "Cherona"
      , rectangle = Rectangle2d.from (Point2d.pixels 960 1334) (Point2d.pixels 1277 1563)
      , epilogues = Epilogues.cherona
      , defaultAuthor = ""
      , defaultEpilogue =
            [ "<p>&emsp;&emsp;When the World Marines came under new leadership, much talk was had about what to do with the \"island\" of Cherona. For centuries, the turtle had housed life, from Ascheron to the marines, but at the end of the day it was a living thing, and as important as the marine base on its back was, many marines began to feel that they were exploiting the turtle.  </p><p>&emsp;&emsp;The turtle seemed to understand instinctively the debate surrounding it, and one day made the choice of what to do very easy: it diverted from its natural paths and began to swim into the Storm, its slow trajectory giving the marines plenty of time to abandon their base. On the day Cherona finally breached the storm, dozens of marines once stationed on it gathered on distant ships to watch with spyglasses as unworldly rains and winds smashed against ancient buildings, tumbling them into the ocean and freeing the great turtle at last. Not one of the marines bore any resentment. </p>"
            ]
      }
    ]
